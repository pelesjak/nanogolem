// ===================================================================
//
// color.h
//     Header file for class representing a color.
//
// Class: CColor
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, November, 1999.

#ifndef __COLOR_H__
#define __COLOR_H__

// Standard headers
#include <cmath>
#include <cassert>

// nanoGOLEM headers
#include "configh.h"
#include "basmath.h"
#include "errorg.h"

__BEGIN_GOLEM_HEADER

// ----------------------------------------------------------
// The current use is specified by the type inside the class
class CColor
{
public:
  // implicit constructor for Color3
  CColor() {
    _vals[0] = _vals[1] = _vals[2] = 0.0;
  }

  CColor(float val) {
    _vals[0] = val; _vals[1] = val; _vals[2] = val;
  }

  CColor(double val) {
    float fval = float(val);
    _vals[0] = fval; _vals[1] = fval; _vals[2] = fval;
  }

  // this is explicit constructor for Color3 given three values
  CColor(float r, float g, float b) {
    _vals[0] = r; _vals[1] = g; _vals[2] = b;
  }

  // copy constructor
  CColor(const CColor &val);

  // Set all 3 values
  void Set(float r, float g, float b) {
    _vals[0] = r; _vals[1] = g; _vals[2] = b;
  }

  // returns the sum of all the components
  inline float Sum() const;

  // returns the sum of all the components
  inline float AbsSum() const;

  // returns the luminance of this color
  inline float Luminance() const;

  // returns the average of all channels
  inline float MeanValue() const;
  
  // assignment operator
  CColor& operator=(const CColor &val);

  // assignment operator
  inline CColor& operator=(const float &val);
  
  // comparison operator
  inline bool operator==(const CColor &val);

  // to get the component for write/read access
  inline float& operator[] (int indx);

  // to get the component for read access only
  inline const float& operator[] (int indx) const;

  // to increase the energy in all components by specified value
  inline CColor& operator += (const float contribution);

  // Multiply the energy in all components by the specified coefficient.
  inline CColor& operator *= (const float mulCoeff);

  // Divide the energy in all components by specified coefficient.
  inline CColor& operator /= (const float divCoeff);
  
  // to add to values .. quick in execution, operands must be of the same type.
  // 1) Color + Color 2) Weigth + Weigth, no other meaning make sense.
  inline CColor& operator += (const CColor &color);


  // the same type.
  // 1) Color - Color 2) Weigth - Weigth, no other meaning make sense.
  inline CColor& operator -= (const CColor &color);

  // to multiply the energy in all components by weight, the only possible
  // interpretation is Color *= weigth. The weigth can have one component
  // only, or the same number of components as the color.
  inline CColor& operator *= (const CColor &weigth);

  /// Divide the energy in all components by weight.
  /**
    The weigth can have one component only, or the same number of components 
    as the color. 
   */
  inline CColor& operator /= (const CColor &weigth);

  // ------------------------------------------------------------------
  // the binary operator functions
  // Add two colors
  friend inline CColor operator+(const CColor &val1, const CColor &val2);

  // Subtract two colors or two weigths. In boths cases operands must be
  // of the same type.
  friend inline CColor operator-(const CColor &val1, const CColor &val2);

  // multiply the colors by weigth or multiply the weigth by weigth.
  // The second argument val2 must have the same number of components
  // as val1, or number of components equal to one
  friend inline CColor operator*(const CColor &val1, const CColor &val2);

  // multiply the colors by float constant
  friend inline CColor operator*(const CColor &val1, const float t);
  friend inline CColor operator*(const float t, const CColor &val1);

  // the input/output operators
  friend ostream& operator<< (ostream &s, const CColor &A);
  // istream& operator>> (istream &s, CColor &A)

private:
  // values of RGB color space
  float _vals[3];
};

// --------------------------------------------------------------------
// inline member functions

// returns the sum of all the components - can be used as Luminance
inline float
CColor::Luminance() const
{
  //int i; // loop variable
  float sum = 0.299f * _vals[0] + // R
    0.587f * _vals[1] + // G
    0.114f * _vals[2]; // B
  return sum;
}

// returns the sum of all the components
inline float
CColor::Sum() const
{
  return _vals[0] + _vals[1] + _vals[2];
}

// returns the sum of all the components
inline float
CColor::AbsSum() const
{
  return fabs(_vals[0]) + fabs(_vals[1]) + fabs(_vals[2]);
}


// returns the sum of all the components - can be used as Luminance
inline float
CColor::MeanValue() const
{
  return Sum()/3.0f;
}

// assignment operator
inline CColor&
CColor::operator=(const float &val)
{
  // now only copy the values to this instance    
  // values in the allocated array
  _vals[0] = _vals[1] = _vals[2] = val;
  return *this; // OK, everything finished
}

// comparison operator
inline bool
CColor::operator==(const CColor &val)
{
  // the common array of values 
  const float *p = _vals;
  const float *q = val._vals;
  for (int i = 3; i--; )
    if ( *p++ != *q++ )
      return false;

  // all the components are the same
  return true;
}

// to get the component for write/read access
inline float&
CColor::operator[] (int index)
{
  assert(index >= 0);
  assert(index < 3);
  return _vals[index]; // the common array of three values 
}

// to get the component for read access only
inline const float&
CColor::operator[] (int index) const
{
  assert(index >= 0);
  assert( index < 3);
  return _vals[index]; // the common array of three values 
}
 

// to increase the energy in all components by specified value
inline CColor&
CColor::operator += (const float contribution)
{
  for (int i = 3; i--; )
    _vals[i] += contribution; // the common array of values
  return *this;
}

// to multiply the energy in all components by specified coefficient
inline CColor&
CColor::operator *= (const float mulCoeff)
{
  for (int i = 3; i--; )
    _vals[i] *= mulCoeff; // the common array of values
  return *this;
}

// --------------------------------------------------------------------------
//  CColor::operator/=
// --------------------------------------------------------------------------
inline CColor& 
CColor::operator /= (const float divCoeff)
{
  return operator *= (1.0f/divCoeff);
}

inline CColor&
CColor::operator += (const CColor &arg)
{
  for (int i = 3;i--;)
    _vals[i] += arg._vals[i]; // the common array of values 
  return *this;
}

inline CColor&
CColor::operator -= (const CColor &arg)
{
  for (int i = 3; i--;)
    _vals[i] -= arg._vals[i]; // the common array of values 
  return *this;
}

inline CColor&
CColor::operator *= (const CColor &weigth)
{
  for (int i = 3; i--; )
    _vals[i] *= weigth._vals[i]; // the common array of values 
  return *this;
}

inline CColor&
CColor::operator /= (const CColor &weight)
{
  const float *src  = weight._vals;
  for (int i = 3; i--; )
    _vals[i] /= src[i];
  return *this;
}

// ------------------------------------------------------------------
// the binary operator functions

inline CColor operator+(const CColor &val1, const CColor &val2)
{
  CColor val = val1;
  return val += val2;
}

inline CColor operator-(const CColor &val1, const CColor &val2)
{
  CColor val = val1;
  return val -= val2;
}

inline CColor operator*(const CColor &val1, const CColor &val2)
{
  CColor val = val1;
  return val *= val2;
}

inline CColor operator*(const CColor &val1, const float t)
{
  CColor val = val1;
  return val *= t;
}

inline CColor operator*(const float t, const CColor &val1)
{
  CColor val = val1;
  return val *= t;
}

__END_GOLEM_HEADER

#endif // __COLOR_H__

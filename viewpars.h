// ===================================================================
//
// viewpars.h
//      Parsing the view format defining the camera
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 2000.

#include <string>

// nanoGOLEM headers
#include "configh.h"

using namespace std;

__BEGIN_GOLEM_HEADER

// Forward declarations
class CWorld;

// Parse VIEW file
bool
CParserVIEWRun(const string &filename, string &newSceneFilename,
	       CWorld &world);

__END_GOLEM_HEADER


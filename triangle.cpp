// ===================================================================
//
// triangle.cpp
//     Source file for Triangle class in nanoGOLEM.
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file
// doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 2000

// nanoGOLEM headers
#include "triangle.h"
#include "basmath.h"
#include "configg.h"
#include "ray.h"
#include "vector3.h"
#include <immintrin.h>
#include <xmmintrin.h>

__BEGIN_GOLEM_SOURCE

void CObject3D::SetCountID(int newCnt) { CObject3D::countID = newCnt; }

int CObject3D::countID = 0;

bool CObject3D::twoSidedPlanars = false;

// Computes a ray-triangle intersection
bool CTriangle::NearestInt(CRay &ray, CHitPointInfo &info) const {

  CVector3D e1 = vertices[1] - vertices[0];
  CVector3D e2 = vertices[2] - vertices[0];
  CVector3D p = CrossProd(ray.GetDir(), e2);
  float a = DotProd(e1, p);

  if (fabs(a) < CLimits::Small)
    return false; // perpendicular, not intersected

  float f = 1.0f / a;
  if ((twoSidedPlanars) && (f < 0.f)) {
    f = -f; // both sides of a triangle are visible
  }
  CVector3D s = ray.GetLoc() - vertices[0];
  CVector3D q = CrossProd(s, e1);
  float t = f * DotProd(e2, q);
  if (t < CLimits::Threshold) // ray goes away from triangle
    return false;             // not intersected

  const float eps = 3e-5f;

  // Using barycentric coordinates with respect to vertex[0]
  float alpha = f * DotProd(s, p);
  if ((alpha < -eps) || (alpha > 1.0f + eps))
    return false; // not intersected

  float beta = f * DotProd(ray.GetDir(), q);

  if ((beta < -eps) || (alpha + beta > 1.0f + eps))
    return false; // not intersected

  if (t < info.GetMaxT()) {
    // Remember the closest intersection
    info.SetT(t);
    info.SetObject(this);
    info.SetMaxT(t);
    info.alpha = alpha;
    info.beta = beta;
    return true; // intersected
  }
  return false; // not intersected
}

// Computes a ray packet-triangle intersection
unsigned CTriangle::NearestInt(CRayPacket &rayPacket,
                               CRPHitPointsInfo &packetInfo) const {

  __m128 cmpResult, notIntersected = _mm_setzero_ps();
  unsigned cmpMask;

  CVector3D e1 = vertices[1] - vertices[0];
  CVector3D e2 = vertices[2] - vertices[0];

  // cross product - cross(rayDirection, e2)
  CVectorSSE3D p = rayPacket.dir.CrossProd(e2);

  // dot product - dot(p, e1)
  __m128 a = p.DotProd(e1);

  // if (fabs(a) < CLimits::Small)
  cmpResult = _mm_cmplt_ps(_mm_abs_ps(a), _mm_set1_ps(CLimits::Small));
  cmpMask = _mm_movemask_ps(cmpResult);
  if (cmpMask) {
    if (cmpMask == _MM_MASK_TRUE) {
      return 0;
    }
    // a = _mm_cndval_ps(cmpResult, _mm_set1_ps(CLimits::Small), a);
    notIntersected = cmpResult;
  }

  // f = 1 / a
  __m128 f = _mm_rcp_ps(a);

  if (twoSidedPlanars) {
    // both sides of a triangle are visible
    f = _mm_abs_ps(f);
  }
  
  CVectorSSE3D s = rayPacket.origin - CVectorSSE3D(vertices[0]);
  CVectorSSE3D q = s.CrossProd(e1);
  
  __m128 t = _mm_mul_ps(f, q.DotProd(e2));

  // if(t < CLimits::Threshold)
  cmpResult = _mm_cmplt_ps(t, _mm_set1_ps(CLimits::Threshold));
  cmpResult = _mm_or_ps(cmpResult, notIntersected);
  cmpMask = _mm_movemask_ps(cmpResult);
  if (cmpMask) {
    if (cmpMask == _MM_MASK_TRUE) {
      return 0;
    }
    notIntersected = cmpResult;
  }

  const float eps = 3e-5f;
  __m128 epsNeg = _mm_set1_ps(-eps);
  __m128 epsPlus1 = _mm_set1_ps(1.0f + eps);

  // Using barycentric coordinates with respect to vertex[0]
  __m128 alpha = _mm_mul_ps(f, DotProd(p, s));

  // if (alpha < -eps) || (alpha > 1.0f + eps)
  cmpResult =
      _mm_or_ps(_mm_cmplt_ps(alpha, epsNeg), _mm_cmpgt_ps(alpha, epsPlus1));
  cmpResult = _mm_or_ps(cmpResult, notIntersected);
  cmpMask = _mm_movemask_ps(cmpResult);
  if (cmpMask) {
    if (cmpMask == _MM_MASK_TRUE) {
      return 0;
    }
    notIntersected = cmpResult;
  }

  __m128 beta = _mm_mul_ps(f, DotProd(rayPacket.dir, q));
  // if (beta < -eps) || (alpha + beta > 1.0f + eps)
  cmpResult = _mm_or_ps(_mm_cmplt_ps(beta, epsNeg),
                        _mm_cmpgt_ps(_mm_add_ps(alpha, beta), epsPlus1));
  cmpResult = _mm_or_ps(cmpResult, notIntersected);
  cmpMask = _mm_movemask_ps(cmpResult);
  if (cmpMask) {
    if (cmpMask == _MM_MASK_TRUE) {
      return 0;
    }
    notIntersected = cmpResult;
  }

  // if(t < maxt)
  cmpResult = _mm_cmplt_ps(t, packetInfo.maxt);
  cmpResult = _mm_andnot_ps(notIntersected, cmpResult);
  cmpMask = _mm_movemask_ps(cmpResult);
  if (cmpMask) {
    // Remember the closest intersection
    packetInfo.t = _mm_cndval_ps(cmpResult, t, packetInfo.t);
    packetInfo.alpha = _mm_cndval_ps(cmpResult, alpha, packetInfo.alpha);
    packetInfo.beta = _mm_cndval_ps(cmpResult, beta, packetInfo.beta);
    packetInfo.maxt = _mm_cndval_ps(cmpResult, t, packetInfo.maxt);

    // Save intersected object for intersecting rays
    if (cmpMask & 1) {
      packetInfo.objects[0] = (CObject3D *)this;
    }
    if ((cmpMask >> 1) & 1) {
      packetInfo.objects[1] = (CObject3D *)this;
    }
    if ((cmpMask >> 2) & 1) {
      packetInfo.objects[2] = (CObject3D *)this;
    }
    if ((cmpMask >> 3) & 1) {
      packetInfo.objects[3] = (CObject3D *)this;
    }

    // Update ray hits
    packetInfo.intersected = packetInfo.intersected | cmpMask;
    return cmpMask; // intersected
  }
  return 0; // not intersected
}

CTriangleNorm::CTriangleNorm(const CVector3D newVertices[3],
                             const CVector3D newNormals[3])
    : CTriangle(newVertices) {
  normals[0] = newNormals[0];
  normals[1] = newNormals[1];
  normals[2] = newNormals[2];
}

CTriangleNorm::CTriangleNorm(const CVector3D newVertices[3],
                             const CVector3D &newNormal)
    : CTriangle(newVertices) {
  normals[0] = newNormal;
  normals[1] = newNormal;
  normals[2] = newNormal;
}

// -----------------------------------------------------------------
// Triangle with normals specified at vertices
void CTriangleNorm::FindNormal(CHitPointInfo &info) const {
  CVector3D N = normals[0] * (1.0f - info.alpha - info.beta) +
                normals[1] * info.alpha + normals[2] * info.beta;
  N.Normalize();
  info.SetNormal(N);
}

CVector3D CTriangleNorm::FindNormal(float alpha, float beta) const {
  CVector3D N = normals[0] * (1.0f - alpha - beta) + normals[1] * alpha +
                normals[2] * beta;
  N.Normalize();
  return N;
}

// -----------------------------------------------------------------
// Triangle with interpolated UV
CTriangleInt::CTriangleInt(const CVector3D newVertices[3],
                           const CVector3D newNormals[3],
                           const CVector2D newUV[3])
    : CTriangleNorm(newVertices, newNormals) {
  for (int i = 0; i < 3; i++) {
    vectorU[i].Set(0.f);
    vectorU[i].Set(0.f);
  }
  uv[0] = newUV[0];
  uv[1] = newUV[1];
  uv[2] = newUV[2];
}

void CTriangleInt::GetUV(CHitPointInfo &info) const {
  CVector2D uvI = uv[0] * (1.0f - info.alpha - info.beta) + uv[1] * info.alpha +
                  uv[2] * info.beta;
  info.SetUV(uvI.xx, uvI.yy);
}

void CTriangleInt::GetUVvectors(CHitPointInfo &info) const {
  FindNormal(info);
  GetUV(info);
  CVector3D U = vectorU[0] * (1.0f - info.alpha - info.beta) +
                vectorU[1] * info.alpha + vectorU[2] * info.beta;
  U.Normalize();
  CVector3D V = vectorV[0] * (1.0f - info.alpha - info.beta) +
                vectorV[1] * info.alpha + vectorV[2] * info.beta;
  V.Normalize();
  info.SetDifferentials(U, V);
}

__END_GOLEM_SOURCE

// ===================================================================
//
// rt00app.h
//     Header file for the project - basic pure ray casting
//
// Class: CRT_00_App
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 2015.

#ifndef __RT00APP_H__
#define __RT00APP_H__

// Standard C++ headers
#include <fstream>

// nanoGOLEM headers
#include "configh.h"
#include "basmath.h"
#include "color.h"
#include "rtcoreca.h"

__BEGIN_GOLEM_HEADER

// forward declarations

class CRT_00_App:
  public CRT_CoreWithCamera_App
{
protected:
  virtual bool RunCommonPrepare();
  // function to return the color for primary rays to be passed to
  // frame sampling scheme .. it is used by CColorMapper in the camera.
  virtual void ComputeColor(const CRay &ray, CHitPointInfo &info,
			    CColor &color);
  // If to show back surfaces of scene triangles
  bool _showBackFaceSurfaces;
  // The value of multiplier for the image
  float _imgHdrMult;

public:
  // constructor
  CRT_00_App(): CRT_CoreWithCamera_App() {}
  // destructor
  virtual ~CRT_00_App() {}

  virtual bool Init(CWorld *world, string &outputName);
  
  // runs the application 
  virtual bool Run();

  // deletes all the auxiliary data structures
  virtual bool CleanUp() { return false;}
};

__END_GOLEM_HEADER

#endif // __RT00APP_H__


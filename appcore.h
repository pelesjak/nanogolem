// ===================================================================
//
// appcore.h
//
//     Header file for application algoritm class.
//
// Class: CSynthesisApp
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 1999

#ifndef __APPCORE_H__
#define __APPCORE_H__

// nanoGOLEM headers
#include "configh.h"
#include "timer.h"

using namespace std;

__BEGIN_GOLEM_HEADER

// forward declarations
class CASDS;
class CEnvironment;
class CObjectList;
class CScene;
class CWorld;
class CRT_01_App;

// -----------------------------------------------------------------------
// This is the core application class - abstract application,
// that can perform any computation in the world over the set of objects.
// -----------------------------------------------------------------------
class CSynthesisApp
{
protected:
  // Note - a world in the application should be always access using a
  // global variable world->, that is the only global variable in the program.

  // environment used by the application instance
  CEnvironment *_environment;
  // the scene with all the objects and other attributes
  CScene       *_scene;
  // object list of the scene for direct access
  CObjectList  *_objects;
  // timer used by this application instance
  CTimer       _timer;

public:
  // constructor
  CSynthesisApp():_environment(0), _scene(0), _objects(0) { }

  
  // explicitely virtual destructor
  virtual ~CSynthesisApp();

  // computes the name of the output, when input is specified
  // and environment is known
  virtual void GetFileName(string &outputFilename, const string &inputFilename,
			   CEnvironment &env) const;

  // Precompute anything what is needed. Returns false on success
  virtual bool Init(CWorld *world, string &outputName);

  // Runs the application, returns false on success. The computed result
  // of the application should be saved within this function.
  virtual bool Run() { return false;}

  // Deletes all the data structures allocated by this algorithm, if the
  // algorithm is run again, it must be a sequence (Init,Run,CleanUp).
  virtual bool CleanUp();

  // This enum provides the knowledge of the output from the algorithm
  // that can be accessed after Run() function.
  enum EOutputDataType {
    EE_Nothing,
    EE_Image,
  };

  // This function returns the type of the output type when needed
  virtual EOutputDataType GetOutputType() const {
    return EE_Nothing;
  }

  // This enum denots exactly the type of the actual application algorithm 
  enum EApplicationType {
    // ------------------------------------------------------------------------
    // the core of ray tracing applications without any casting of anything
    EE_RT_Core = 0,
    // the classical recursive ray tracer as defined by Whitted, 1980
    EE_ClassicalRayTracer1980 = 1,
  };
  
  // returns the ASDS if it is constructed within the application. If not
  // returns NULL (default behaviour)
  virtual const CASDS* GetASDS() const { return NULL;}
  
  // sets some preferences that could be required by the parser when
  // reading the scene file or other part of the library before executing
  // the application.
  virtual void SetPreferences(CEnvironment &env);

  // returns timer for reading only
  const CTimer& GetTimer() const {return _timer;}

  // returns objects for reading only
  const CObjectList* GetObjectList() const {return _objects;}
  
  // ------------------------------------------------------------------
  // statistics related things
  
  // reports the statistics result of the specific application
  virtual bool DoReport(const CWorld &world, ostream &app);

  // ----------------------------------------------------------------------
  // Helper function that creates the application according to its name
  // In case of failure returns NULL.
  static CSynthesisApp* CreateApplication(const string &applicationName);
};

// --------------------------------------------------------------------
// Each application type class derived from CSynthesisApp class
//  should be placed in the separate file:
//
//   imgcore.h .. the header file for image synthesis application(s)
//   movicore.h .. the header file for movie synthesis application(s)
//   sndcore.h .. the header for sound synthesis application(s)
//   etc.
// --------------------------------------------------------------------

// This is the external application allocation function. This is required
// to avoid problem with the allocation of the applications that are
// defined outside the library.
extern CSynthesisApp*
GlobalAllocApplication1(const string &applicationName);

// Similar allocation function for students of Jaroslav Krivanek, 2006
extern CSynthesisApp*
GlobalAllocApplication2(const string &applicationName);

// allocates application by a different mechanism
extern CSynthesisApp* AllocRT_1_App();

__END_GOLEM_HEADER

#endif // __APPCORE_H__


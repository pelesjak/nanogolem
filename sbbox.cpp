// ===================================================================
//
// sbbox.cpp
//     Simply bounding box for ray tracing only, 24 Bytes in size.
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file
// doc/Licence.txt
//
// Initial coding by Vlasta Havran, December 2005

// nanoGOLEM headers
#include "sbbox.h"
#include "basstr.h"
#include "configg.h"
#include "sseutils.h"
#include <immintrin.h>
#include <xmmintrin.h>

__BEGIN_GOLEM_SOURCE

// Overload << operator for C++-style output
ostream &operator<<(ostream &s, const SBBox &A) {
  return s << '[' << A.pp[0].x << ", " << A.pp[0].y << ", " << A.pp[0].z << "]["
           << A.pp[1].x << ", " << A.pp[1].y << ", " << A.pp[1].z << ']';
}

// Overload >> operator for C++-style input
istream &operator>>(istream &s, SBBox &A) {
  char a;
  // read "[min.x, min.y, min.z][max.x, max.y, max.z]"
  return s >> a >> A.pp[0].x >> a >> A.pp[0].y >> a >> A.pp[0].z >> a >> a >>
         A.pp[1].x >> a >> A.pp[1].y >> a >> A.pp[1].z >> a;
}

void SBBox::operator=(const SBBox &box) {
  pp[0].x = box.pp[0].x;
  pp[0].y = box.pp[0].y;
  pp[0].z = box.pp[0].z;
  pp[1].x = box.pp[1].x;
  pp[1].y = box.pp[1].y;
  pp[1].z = box.pp[1].z;
}

void Describe(const SBBox &b, ostream &app, int ind) {
  Indent(app, ind);
  app << "SBBox: min at(" << b.Min() << "), max at(" << b.Max() << ")\n";
}

// Function describing the box
void DescribeXYZ(const SBBox &b, ostream &app, int ind) {
  Indent(app, ind);
  app << " box = ( " << b.Min().x << " , " << b.Max().x << " )( " << b.Min().y
      << " , " << b.Max().y << " )( " << b.Min().z << " , " << b.Max().z
      << " )\n";
}

bool SBBox::Includes(const SBBox &b) const {
  if (b.pp[0].x >= pp[0].x && b.pp[0].y >= pp[0].y && b.pp[0].z >= pp[0].z &&
      b.pp[1].x <= pp[1].x && b.pp[1].y <= pp[1].y && b.pp[1].z <= pp[1].z)
    return true;
  return false;
}

bool SBBox::Includes(const SBBox &b, float eps) const {
  if (b.pp[0].x >= pp[0].x - eps && b.pp[0].y >= pp[0].y - eps &&
      b.pp[0].z >= pp[0].z - eps && b.pp[1].x <= pp[1].x + eps &&
      b.pp[1].y <= pp[1].y + eps && b.pp[1].z <= pp[1].z + eps)
    return true;
  return false;
}

// The implementation I, the first version implemented by Vlastimil
// Havran, without looking into the literature
bool SBBox::RayIntersect(const CRay &ray, float &tmin, float &tmax) {

  float interval_min = tmin;
  float interval_max = tmax;

  const CVector3D &rayLoc = ray.GetLoc();
  CVector3D rayDir = ray.GetDir();

  float t0, t1;
  const float eps = 1e-6f;
  if (rayDir.x > eps) {
    t0 = (pp[0].x - rayLoc.x) / rayDir.x;
    t1 = (pp[1].x - rayLoc.x) / rayDir.x;
  } else if (rayDir.x < -eps) {
    t0 = (pp[1].x - rayLoc.x) / rayDir.x;
    t1 = (pp[0].x - rayLoc.x) / rayDir.x;
  } else {
    t0 = (pp[1].x - rayLoc.x) / eps;
    t1 = (pp[0].x - rayLoc.x) / eps;
    if (t0 > t1)
      swap(t0, t1);
  }

  assert(t0 <= t1);
  if (t0 > interval_min)
    interval_min = t0;
  if (t1 < interval_max)
    interval_max = t1;
  if (interval_min > interval_max)
    return false;

  if (rayDir.y > eps) {
    t0 = (pp[0].y - rayLoc.y) / rayDir.y;
    t1 = (pp[1].y - rayLoc.y) / rayDir.y;
  } else if (rayDir.y < -eps) {
    t0 = (pp[1].y - rayLoc.y) / rayDir.y;
    t1 = (pp[0].y - rayLoc.y) / rayDir.y;
  } else {
    t0 = (pp[1].y - rayLoc.y) / eps;
    t1 = (pp[0].y - rayLoc.y) / eps;
    if (t0 > t1)
      swap(t0, t1);
  }

  assert(t0 <= t1);
  if (t0 > interval_min)
    interval_min = t0;
  if (t1 < interval_max)
    interval_max = t1;
  if (interval_min > interval_max)
    return false;

  if (rayDir.z > eps) {
    t0 = (pp[0].z - rayLoc.z) / rayDir.z;
    t1 = (pp[1].z - rayLoc.z) / rayDir.z;
  } else if (rayDir.z < -eps) {
    t0 = (pp[1].z - rayLoc.z) / rayDir.z;
    t1 = (pp[0].z - rayLoc.z) / rayDir.z;
  } else {
    t0 = (pp[1].z - rayLoc.z) / eps;
    t1 = (pp[0].z - rayLoc.z) / eps;
    if (t0 > t1)
      swap(t0, t1);
  }

  assert(t0 <= t1);
  if (t0 > interval_min)
    interval_min = t0;
  if (t1 < interval_max)
    interval_max = t1;

  // return true if the box is intersected
  // return false if not intersected by ray
  if ((interval_max > 0.0f) && (interval_min <= interval_max)) {
    // yes, intersected, update tmin and tmax for current node
    tmin = interval_min;
    tmax = interval_max;
    return true; // intersected
  }
  return false; // not intersected
}

unsigned SBBox::RayPacketIntersect(CRayPacket &rayPacket, __m128 &tmin,
                                   __m128 &tmax) {

  __m128 interval_min = tmin;
  __m128 interval_max = tmax;
  __m128 temp0, temp1;

  const CVectorSSE3D &rayLoc = rayPacket.origin;

  __m128 t0, t1, cmpResult, divider, notIntersected = _mm_setzero_ps();
  unsigned cmpMask;

  __m128 eps = _mm_set1_ps(1e-6f);
  __m128 epsNeg = _mm_set1_ps(-1e-6f);

  // X direction
  // if (dir.x > 0)
  cmpResult = _mm_cmpgt_ps(rayPacket.dir.x, _mm_setzero_ps());
  // divider = _mm_cndval_ps(cmpResult, rayPacket.dir.x, eps);
  temp0 = _mm_sub_ps(_mm_set1_ps(pp[0].x), rayLoc.x);
  temp1 = _mm_sub_ps(_mm_set1_ps(pp[1].x), rayLoc.x);

  t0 = _mm_div_ps(_mm_cndval_ps(cmpResult, temp0, temp1), rayPacket.dir.x);
  t1 = _mm_div_ps(_mm_cndval_ps(cmpResult, temp1, temp0), rayPacket.dir.x);

  // if (t0 > interval_min)
  cmpResult = _mm_cmpgt_ps(t0, interval_min);
  if (_mm_movemask_ps(cmpResult)) {
    // interval_min = t0
    interval_min = _mm_cndval_ps(cmpResult, t0, interval_min);
  }

  // if (t1 < interval_max)
  cmpResult = _mm_cmplt_ps(t1, interval_max);
  if (_mm_movemask_ps(cmpResult)) {
    // interval_max = t1
    interval_max = _mm_cndval_ps(cmpResult, t1, interval_max);
  }

  // if (interval_min > interval_max)
  cmpResult = _mm_cmpgt_ps(interval_min, interval_max);
  cmpMask = _mm_movemask_ps(cmpResult);
  if (cmpMask) {
    if (cmpMask == _MM_MASK_TRUE) {
      return 0;
    }
    notIntersected = cmpResult;
  }

  // Y direction
  // if (fabs(dir.y) > eps)
  cmpResult = _mm_cmpgt_ps(rayPacket.dir.y, _mm_setzero_ps());
  // divider = _mm_cndval_ps(cmpResult, rayPacket.dir.x, eps);
  temp0 = _mm_sub_ps(_mm_set1_ps(pp[0].y), rayLoc.y);
  temp1 = _mm_sub_ps(_mm_set1_ps(pp[1].y), rayLoc.y);

  t0 = _mm_div_ps(_mm_cndval_ps(cmpResult, temp0, temp1), rayPacket.dir.y);
  t1 = _mm_div_ps(_mm_cndval_ps(cmpResult, temp1, temp0), rayPacket.dir.y);

  // if (t0 > interval_min)
  cmpResult = _mm_cmpgt_ps(t0, interval_min);
  if (_mm_movemask_ps(cmpResult)) {
    // interval_min = t0
    interval_min = _mm_cndval_ps(cmpResult, t0, interval_min);
  }

  // if (t1 < interval_max)
  cmpResult = _mm_cmplt_ps(t1, interval_max);
  if (_mm_movemask_ps(cmpResult)) {
    // interval_max = t1
    interval_max = _mm_cndval_ps(cmpResult, t1, interval_max);
  }

  // if (interval_min > interval_max)
  cmpResult = _mm_cmpgt_ps(interval_min, interval_max);
  cmpResult = _mm_or_ps(notIntersected, cmpResult);
  cmpMask = _mm_movemask_ps(cmpResult);
  if (cmpMask) {
    if (cmpMask == _MM_MASK_TRUE) {
      return 0;
    }
    notIntersected = cmpResult;
  }

  // Z direction
  // if (fabs(dir.z) > eps)
  cmpResult = _mm_cmpgt_ps(rayPacket.dir.z, _mm_setzero_ps());
  // divider = _mm_cndval_ps(cmpResult, rayPacket.dir.x, eps);
  temp0 = _mm_sub_ps(_mm_set1_ps(pp[0].z), rayLoc.z);
  temp1 = _mm_sub_ps(_mm_set1_ps(pp[1].z), rayLoc.z);

  t0 = _mm_div_ps(_mm_cndval_ps(cmpResult, temp0, temp1), rayPacket.dir.z);
  t1 = _mm_div_ps(_mm_cndval_ps(cmpResult, temp1, temp0), rayPacket.dir.z);

  // if (t0 > interval_min)
  cmpResult = _mm_cmpgt_ps(t0, interval_min);
  if (_mm_movemask_ps(cmpResult)) {
    // interval_min = t0
    interval_min = _mm_cndval_ps(cmpResult, t0, interval_min);
  }

  // if (t1 < interval_max)
  cmpResult = _mm_cmplt_ps(t1, interval_max);
  if (_mm_movemask_ps(cmpResult)) {
    // interval_max = t1
    interval_max = _mm_cndval_ps(cmpResult, t1, interval_max);
  }

  // return true if the box is intersected
  // return false if not intersected by ray
  // if ((interval_max > 0.0f) && (interval_min <= interval_max))
  cmpResult = _mm_and_ps(_mm_cmpgt_ps(interval_max, _mm_setzero_ps()),
                         _mm_cmple_ps(interval_min, interval_max));
  cmpResult = _mm_andnot_ps(notIntersected, cmpResult);
  unsigned res = _mm_movemask_ps(cmpResult);
  if (res) {
    // yes, intersected, update tmin and tmax for current node
    tmin = _mm_cndval_ps(cmpResult, interval_min, tmin);
    tmax = _mm_cndval_ps(cmpResult, interval_max, tmax);
    return res; // intersected
  }

  return 0; // not intersected
}

__END_GOLEM_SOURCE

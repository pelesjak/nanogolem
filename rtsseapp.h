#ifndef __RTSSEAPP_H__
#define __RTSSEAPP_H__

// Standard C++ headers
#include <fstream>

// nanoGOLEM headers
#include "basmath.h"
#include "color.h"
#include "configh.h"
#include "rtcoreca.h"

__BEGIN_GOLEM_HEADER

// forward declarations

class CRT_SSE_App : public CRT_CoreWithCamera_App {
protected:
  virtual bool RunCommonCompute();
  virtual void ComputeColor(CRayPacket &rayPacket, CRPHitPointsInfo &packetInfo,
                            CColor* colors);
  // If to show back surfaces of scene triangles
  bool _showBackFaceSurfaces;
  // The value of multiplier for the image
  float _imgHdrMult;

public:
  // constructor
  CRT_SSE_App() : CRT_CoreWithCamera_App() {}
  // destructor
  virtual ~CRT_SSE_App() {}

  virtual bool Init(CWorld *world, string &outputName);

  // runs the application
  virtual bool Run();

  // deletes all the auxiliary data structures
  virtual bool CleanUp() { return false; }
};

__END_GOLEM_HEADER

#endif // __RTSSEAPP_H__
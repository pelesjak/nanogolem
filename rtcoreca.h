// ===================================================================
//
// rtcoreca.h
//     Header file for ray tracing application including camera
//
// Class: CRT_CoreWithCamera_App
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, December 2000

#ifndef __RTCORECA_H__
#define __RTCORECA_H__

// nanoGOLEM headers
#include "configh.h"
#include "camera.h"
#include "hdrimg.h"
#include "rtcore.h"

__BEGIN_GOLEM_HEADER

// forward declarations
class CASDS;
class CObject3D;
class CRay;

// -------------------------------------------------------------------------
// This is the core class of rendering algorithms casting rays from camera
// -------------------------------------------------------------------------
class CRT_CoreWithCamera_App:
  public CRT_Core_App
{
protected:
  // The resulting image
  CHDRImage *_image;
  // The visualization image
  CHDRImage *_imageVis;

  // The definition of the perspective camera to be used
  CCamera   _camera;

  // how many primary rays hits
  int primHit;
  // how many primary rays hits
  int primMiss;
  // When saving evaluation image, what should be put there
  float evalValueVIS;
  // For the whole image
  SVAR statsAVG;
  
  /// Save HDR image without tonemapping.
  bool SaveImage(bool hdrFlag);
  
  // Creates the camera, computes the image, records the statistics data,
  // and saves the image, possibly tonemapped and/or HDR.
  // Return false, if everything was ok. For failure returns true.
  bool RunCommon();

  // Only prepares the camera and other variables before rendering
  bool RunCommonPrepare();
  // Computes the image with the built up camera
  bool RunCommonCompute();
  // Performs tonemapping, saves the image (HDR and tonemapped),
  // and deallocate the variables for camera and colorMapper
  // It makes tone mapping as default, which can be switched off
  bool RunCommonFinish(bool makeToneMapping = true);

  // Initialize the camera parameters
  bool InitCamera();
  // Compute the color for a primary ray from the camera
  virtual void ComputeColor(const CRay &, CHitPointInfo &,
			    CColor &/*color*/);
public:
  // constructor
  CRT_CoreWithCamera_App();
  // destructor
  virtual ~CRT_CoreWithCamera_App();
  
  // precompute anything what is needed
  // returns false, if everything is OK
  virtual bool Init(CWorld *world, string &outputName);
  // deletes all the auxiliary data structures
  virtual bool CleanUp();
  // runs the application, in this common class do nothing
  virtual bool Run() { return false;}
  // reports the statistics result of the specific application
  virtual bool DoReport(const CWorld &world, ostream &app);

  // returns application type, must be defined by the application
  virtual EApplicationType GetApplicationType() const {
    return EE_RT_Core;
  }

  struct SSpherePoint {
	  CVector3D point;
	  int n;
	  double prevDist;
	  double minDist;
	  double angle;
  };

  struct SBoundSphere {
	  CVector3D center;
	  double radius;
  };

  typedef vector<SSpherePoint *> TSpherePointList;

  int GenerateSpherePoints(TSpherePointList &alist, int N, int BN, double coeff, bool constSA);
  void GenerateBoundingSphere(CASDS * asds, SBoundSphere *bs, CObjectList &alist);
};

__END_GOLEM_HEADER

#endif // __RTCORECA_H__


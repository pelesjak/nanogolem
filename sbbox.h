// ===================================================================
//
// sbbox.h
//      SBBox - bounding box by 3 min and 3 max coordinates
//
// Class: SBBox
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file
// doc/Licence.txt
//
// Initial coding by Vlasta Havran, December 2005

#ifndef __SBBOX_H__
#define __SBBOX_H__

// ANSI C++ headers
#include <cassert>
#include <immintrin.h>
#include <iostream>
#include <ostream>

// nanoGOLEM headers
#include "basmath.h"
#include "configh.h"
#include "ray.h"
#include "sseutils.h"
#include "vector3.h"

__BEGIN_GOLEM_HEADER

// Forward declarations
class CRay;

// --------------------------------------------------------
// The definition of the bounding box having 24 Bytes only
struct SBBox {
public:
  SBBox() {}
  SBBox(const CVector3D &a, const CVector3D &b) {
    pp[0] = a;
    pp[1] = b;
  }
  // member functions
  const CVector3D &Min() const { return pp[0]; }
  const CVector3D &Max() const { return pp[1]; }
  CVector3D &Min() { return pp[0]; }
  CVector3D &Max() { return pp[1]; }
  const float &Min(int i) const { return pp[0][i]; }
  const float &Max(int i) const { return pp[1][i]; }
  float &Min(int i) { return pp[0][i]; }
  float &Max(int i) { return pp[1][i]; }

  // Access to individual components, maybe it is not a good idea
  const float &MinX() const { return pp[0].x; }
  const float &MaxX() const { return pp[1].x; }
  float &MinX() { return pp[0].x; }
  float &MaxX() { return pp[1].x; }
  const float &MinY() const { return pp[0].y; }
  const float &MaxY() const { return pp[1].y; }
  float &MinY() { return pp[0].y; }
  float &MaxY() { return pp[1].y; }
  const float &MinZ() const { return pp[0].z; }
  const float &MaxZ() const { return pp[1].z; }
  float &MinZ() { return pp[0].z; }
  float &MaxZ() { return pp[1].z; }

  void SetMin(float x, float y, float z) {
    pp[0].x = x;
    pp[0].y = y;
    pp[0].z = z;
  }
  void SetMax(float x, float y, float z) {
    pp[1].x = x;
    pp[1].y = y;
    pp[1].z = z;
  }

  // initialization to the non existing bounding box
  inline void Initialize() {
    pp[0] = CVector3D(MAXFLOAT);
    pp[1] = CVector3D(-MAXFLOAT);
  }
  CVector3D Diagonal() const { return pp[1] - pp[0]; }

  // Compute the center of the box
  void ComputeCentroid(CVector3D &c) const {
    c.x = (pp[0].x + pp[1].x) * 0.5f;
    c.y = (pp[0].y + pp[1].y) * 0.5f;
    c.z = (pp[0].z + pp[1].z) * 0.5f;
  }

  CVector3D ComputeCentroid() {
    return CVector3D((pp[0].x + pp[1].x) * 0.5f, (pp[0].y + pp[1].y) * 0.5f,
                     (pp[0].z + pp[1].z) * 0.5f);
  }

  // Compute half of surface area of the box
  float SA2() const {
    float t1 = (pp[1].x - pp[0].x);
    float t2 = (pp[1].y - pp[0].y);
    float t3 = (pp[1].z - pp[0].z);
    return t1 * t2 + t2 * t3 + t1 * t3;
  }
  // Compute volume of the box
  float GetVolume() const {
    return (pp[1].x - pp[0].x) * (pp[1].y - pp[0].y) * (pp[1].z - pp[0].z);
  }

  void SetMin(int axis, const float value) { pp[0][axis] = value; }
  void SetMax(int axis, const float value) { pp[1][axis] = value; }
  // Write acess to min and max
  void SetMin(const CVector3D &bmin) { pp[0] = bmin; }
  void SetMax(const CVector3D &bmax) { pp[1] = bmax; }

  // Decrease box by given splitting plane
  void Reduce(int axis, int right, float value) {
    if ((value >= pp[0][axis]) && (value <= pp[1][axis])) {
      if (right)
        pp[0][axis] = value;
      else
        pp[1][axis] = value;
    }
  }
  inline void Include(const SBBox &bbox);
  bool RayIntersect(const CRay &r, float &tmin, float &tmax);
  unsigned RayPacketIntersect(CRayPacket &rayPacket, __m128 &tmin,
                              __m128 &tmax);
  // Query functions

  // Includes returns true if this box includes box b (completely)
  bool Includes(const SBBox &b) const;
  bool Includes(const SBBox &b, float eps) const;

  // Returns false if the box 'b' is fully contained in 'this', see the code
  inline bool ExcludesPartially(const SBBox &b) const;

  // Returs true if this box includes a point
  inline bool Includes(const CVector3D &vec) const;

  // Includes returns true if this box includes box b including boundary
  inline bool IncludesS(const CVector3D &vec) const;

  inline bool IncludesS(const CVector3D &vec, float eps) const;

  inline bool Equal(const SBBox &b, float eps = 0.f) const;

  // Test if the box is really sensefull
  bool IsCorrect();

  // Overlap returns 1 if the two axis-aligned boxes overlap .. only strongly
  friend inline bool OverlapS(const SBBox &, const SBBox &);

  friend ostream &operator<<(ostream &s, const SBBox &A);
  friend istream &operator>>(istream &s, SBBox &A);
  void operator=(const SBBox &box);

protected:
  // pp[0] is minimum, pp[1] is maximum
  CVector3D pp[2];
  friend class CTDBVH;
};

// --------------------------------------------------------
// Inline functions

inline void SBBox::Include(const SBBox &bbox) {
  Minimize(pp[0], bbox.pp[0]);
  Maximize(pp[1], bbox.pp[1]);
}

inline bool SBBox::Equal(const SBBox &b, float eps) const {
  if ((!EpsilonEqual(pp[0].x, b.pp[0].x, eps)) ||
      (!EpsilonEqual(pp[0].y, b.pp[0].y, eps)) ||
      (!EpsilonEqual(pp[0].z, b.pp[0].z, eps)) ||
      (!EpsilonEqual(pp[1].x, b.pp[1].x, eps)) ||
      (!EpsilonEqual(pp[1].y, b.pp[1].y, eps)) ||
      (!EpsilonEqual(pp[1].z, b.pp[1].z, eps)))
    return false; // they are not equal
  return true;
}

inline bool SBBox::IsCorrect() {
  if ((pp[0].x > pp[1].x) || (pp[0].y > pp[1].y) || (pp[0].z > pp[1].z))
    return false; // box is not formed
  return true;
}

inline bool OverlapS(const SBBox &x, const SBBox &y) {
  if (x.pp[1].x <= y.pp[0].x || x.pp[0].x >= y.pp[1].x ||
      x.pp[1].y <= y.pp[0].y || x.pp[0].y >= y.pp[1].y ||
      x.pp[1].z <= y.pp[0].z || x.pp[0].z >= y.pp[1].z) {
    return false;
  }
  return true;
}

// Returns true if the boxes are either disjoint or only
// share a single face !
inline bool SBBox::ExcludesPartially(const SBBox &b) const {
  if (b.pp[1].x <= pp[0].x || b.pp[1].y <= pp[0].y || b.pp[1].z <= pp[0].z ||
      b.pp[0].x >= pp[1].x || b.pp[0].y >= pp[1].y || b.pp[0].z >= pp[1].z)
    // box 'b' is not fully contained in 'this', also true when neighboring
    // faces
    return true;
  return false; // box 'b' contained in this box
}

inline bool SBBox::Includes(const CVector3D &vec) const {
  if (vec.x < pp[0].x || vec.x > pp[1].x || vec.y < pp[0].y ||
      vec.y > pp[1].y || vec.z < pp[0].z || vec.z > pp[1].z) {
    return false;
  }
  return true;
}

inline bool SBBox::IncludesS(const CVector3D &vec) const {
  if (vec.x <= pp[0].x || vec.x >= pp[1].x || vec.y <= pp[0].y ||
      vec.y >= pp[1].y || vec.z <= pp[0].z || vec.z >= pp[1].z) {
    return false;
  }
  return true;
}

inline bool SBBox::IncludesS(const CVector3D &vec, float eps) const {
  if (vec.x <= (pp[0].x - eps) || vec.x >= (pp[1].x + eps) ||
      vec.y <= (pp[0].y - eps) || vec.y >= (pp[1].y + eps) ||
      vec.z <= (pp[0].z - eps) || vec.z >= (pp[1].z + eps)) {
    return false; // outside
  }
  return true; // point inside
}

// Function describing the box
extern void Describe(const SBBox &b, ostream &app, int indent);

// Function describing the box
extern void DescribeXYZ(const SBBox &b, ostream &app, int indent);

// Bounding box for an object, and it is AABB
struct BoundingBox : public SBBox {
  int object; // bounded object, remember one object can have more b. boxes
  BoundingBox() {}
  BoundingBox(const CVector3D &a, const CVector3D &b, int objectIndex)
      : SBBox(a, b), object(objectIndex) {}
};

__END_GOLEM_HEADER

#endif // __SBBOX_H__

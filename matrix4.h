// ===================================================================
//
// matrix4.h
//	Header file for CMatrix4 class in nanoGOLEM.
//
// Class: CMatrix4
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 2000.

#ifndef __MATRIX4_H__
#define __MATRIX4_H__

// nanoGOLEM headers
#include "configh.h"
#include "vector3.h"

__BEGIN_GOLEM_HEADER

// Forward declarations
class CVector3D;

class CMatrix4
{
public:
  union {
    // first index is column [x], the second is row [y]
    float x[4][4]; 
    float l[16];
  }; 

  CMatrix4() { }

  // here xXY - 'X' is row, and 'Y' is column - classical mathematical notation
  CMatrix4(float x11, float x12, float x13, float x14,
	   float x21, float x22, float x23, float x24,
	   float x31, float x32, float x33, float x34,
	   float x41, float x42, float x43, float x44);
  
  // cast to float*
  operator float* () { return &x[0][0]; }
  operator const float* () const { return &x[0][0]; }

  // access to matrix components
  float& operator[] (int idx) { return l[idx]; }
  const float& operator[] (int idx) const { return l[idx]; }

  /// fisrt index row, second index column
  float& operator() (int i,int j) { return x[j][i]; }
  const float& operator() (int i,int j) const { return x[j][i]; } 

  inline void Set(
    float x00,float x01,float x02,float x03,
    float x10,float x11,float x12,float x13,
    float x20,float x21,float x22,float x23,
    float x30,float x31,float x32,float x33); 

  /// set the current matrix to identity
  inline void SetIdentity(void);

  /// Assignment operators
  //@{
  /// add-to
  CMatrix4& operator+= (const CMatrix4 &A);
  /// subtract-from
  CMatrix4& operator-= (const CMatrix4 &A);
  /// multiply by matrix
  CMatrix4& operator*= (const CMatrix4 &A);
  /// scale by scalar
  CMatrix4& operator*= (float A);
  //@}

  /// Fundamental operations
  int Invert();		      // Invert the matrix .. returns 0 = regular
  void Transpose();				// Transpose the matrix
  friend CMatrix4 Invert(const CMatrix4 &M);	// Invert a given matrix
  friend CMatrix4 Transpose(const CMatrix4 &M);	// Transpose a given matrix

  // Create various types of matrix.
  friend CMatrix4 IdentityMatrix();
  friend CMatrix4 ZeroMatrix();
  friend CMatrix4 TranslationMatrix(const CVector3D &Location);
  friend CMatrix4 RotationXMatrix(float Angle);
  friend CMatrix4 RotationYMatrix(float Angle);
  friend CMatrix4 RotationZMatrix(float Angle);
  friend CMatrix4 RotationYPRMatrix(float Yaw, float Pitch, float Roll);
  // about axis 'axis' by angle 'Angle'
  friend CMatrix4 RotationAxisMatrix(const CVector3D &axis, float Angle);
  // create the rotation matrix that rotates 'vecFrom' to 'vecTo'
  friend CMatrix4 RotationVectorsMatrix(const CVector3D &vecFrom,
					const CVector3D &vecTo);
  
  friend CMatrix4 ScaleMatrix(float X, float Y, float Z);
  friend CMatrix4 GenRotation(const CVector3D &x, const CVector3D &y,
			     const CVector3D &z);
  friend CMatrix4 QuadricMatrix(float a, float b, float c, float d, float e,
			      float f, float g, float h, float j, float k);
  // returns matrix for transforming normal
  friend CMatrix4 NormalTransformMatrix(const CMatrix4 &M);

  friend CMatrix4 MirrorX();
  friend CMatrix4 MirrorY();
  friend CMatrix4 MirrorZ();
  friend CMatrix4 RotationOnly(const CMatrix4 &x);

  // Binary operators
  friend CMatrix4 operator+ (const CMatrix4 &A, const CMatrix4 &B);
  friend CMatrix4 operator- (const CMatrix4 &A, const CMatrix4 &B);
  friend CMatrix4 operator* (const CMatrix4 &A, float B);
  friend CMatrix4 operator* (const CMatrix4 &A, const CMatrix4 &B);

  // friends returning CVector3D
  friend CVector3D operator*(const CMatrix4 &M, const CVector3D &v);
  friend CVector3D RotateOnly(const CMatrix4 &M, const CVector3D &v);
  friend CVector3D TransformNormal(const CMatrix4 &M, const CVector3D &v);
  friend CVector3D GetTranslation(const CMatrix4 &M);

  // Overloaded output operator.
  friend ostream& operator<< (ostream &s, const CMatrix4 &M);
};

// --------------------------------------------------------------------------
//  CMatrix4::Set()
// --------------------------------------------------------------------------
inline void 
CMatrix4::Set(float x00,float x01,float x02,float x03,
              float x10,float x11,float x12,float x13,
              float x20,float x21,float x22,float x23,
              float x30,float x31,float x32,float x33)
{
  CMatrix4 &M=*this;
  M(0,0)=x00 ; M(0,1)=x01 ; M(0,2)=x02 ; M(0,3)=x03 ;
  M(1,0)=x10 ; M(1,1)=x11 ; M(1,2)=x12 ; M(1,3)=x13 ;
  M(2,0)=x20 ; M(2,1)=x21 ; M(2,2)=x22 ; M(2,3)=x23 ;
  M(3,0)=x30 ; M(3,1)=x31 ; M(3,2)=x32 ; M(3,3)=x33 ;
} 

// --------------------------------------------------------------------------
//  CMatrix4::SetIdentity()
// --------------------------------------------------------------------------
inline void 
CMatrix4::SetIdentity(void)
{
  Set(1 , 0 , 0 , 0 ,
      0 , 1 , 0 , 0 ,
      0 , 0 , 1 , 0 ,
      0 , 0 , 0 , 1  );
} 

// Create various types of matrix.
CMatrix4 IdentityMatrix();
CMatrix4 ZeroMatrix();
CMatrix4 TranslationMatrix(const CVector3D &Location);
CMatrix4 RotationXMatrix(float Angle);
CMatrix4 RotationYMatrix(float Angle);
CMatrix4 RotationZMatrix(float Angle);
CMatrix4 RotationYPRMatrix(float Yaw, float Pitch, float Roll);
CMatrix4 RotationAxisMatrix(const CVector3D &axis, float Angle);
CMatrix4 RotationVectorsMatrix(const CVector3D &vecFrom,
			       const CVector3D &vecTo);
  
CMatrix4 ScaleMatrix(float X, float Y, float Z);
CMatrix4 GenRotation(const CVector3D &x, const CVector3D &y,
		     const CVector3D &z);
CMatrix4 QuadricMatrix(float a, float b, float c, float d, float e,
		       float f, float g, float h, float j, float k);
// returns matrix for transforming normal
CMatrix4 NormalTransformMatrix(const CMatrix4 &M);

CMatrix4 MirrorX();
CMatrix4 MirrorY();
CMatrix4 MirrorZ();
CMatrix4 RotationOnly(const CMatrix4 &x);

// Binary operators
CMatrix4 operator+ (const CMatrix4 &A, const CMatrix4 &B);
CMatrix4 operator- (const CMatrix4 &A, const CMatrix4 &B);
CMatrix4 operator* (const CMatrix4 &A, float B);
CMatrix4 operator* (const CMatrix4 &A, const CMatrix4 &B);

// friends returning CVector3D
CVector3D operator*(const CMatrix4 &M, const CVector3D &v);
CVector3D RotateOnly(const CMatrix4 &M, const CVector3D &v);
CVector3D TransformNormal(const CMatrix4 &M, const CVector3D &v);
CVector3D GetTranslation(const CMatrix4 &M);

__END_GOLEM_HEADER

#endif // __MATRIX4X4_H__


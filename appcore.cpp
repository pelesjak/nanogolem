// ===================================================================
//
// appcore.cpp
//             The core of synthesis algorithm
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file
// doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 1999-2000

// standard headers
#include <iomanip>

// nanoGOLEM headers
#include "appcore.h"
#include "basstr.h"
#include "configg.h"
#include "rtcore.h"
#include "scene.h"
#include "world.h"

// Exclude the application from the compilations
#ifndef __EXCLUDE_APPS

// Headers for ray tracing applications
#include "rt00app.h"
// 2x2 ray packets with SSE instructions
#include "rtsseapp.h"

#endif // __EXCLUDE_APPS

__BEGIN_GOLEM_SOURCE

// precompute anything what is needed
// returns false, if everything is OK
bool CSynthesisApp::Init(CWorld *world, string &outputName) {
  CleanUp();
  _timer.Reset();

  if (outputName.length() == 0)
    outputName.assign("default");

  // set the classes for direct access
  if (world) {
    // environment used by the application instance
    _environment = &(world->_environment);

    // the scene with all the objects and other attributes
    _scene = world->_scene;
    if (_scene) {
      // object list of the scene for direct access
      _objects = &(_scene->objects);
    }

    return false; // OK
  }

  return true; // error
}

CSynthesisApp::~CSynthesisApp() { CleanUp(); }

bool CSynthesisApp::CleanUp() {
  _scene = NULL;
  _objects = NULL;
  _environment = NULL;

  return false;
}

void CSynthesisApp::SetPreferences(CEnvironment & /*env*/) {
  // Anything can be set here
  // env.SetString("XXX.YYY", "VALUE");
}

void CSynthesisApp::GetFileName(string &outputName, const string &inputName,
                                CEnvironment &env) const {
  // Get the output file name
  if (env.OptionPresent("OutputFileName")) {
    env.GetString("OutputFileName", outputName);

    // check if user didn't disable the output file by "-O"
  } else {
    // derive the name from the input name
    string outType;
    env.GetString("OutputFileType", outType);
    ChangeFilenameExtension(inputName, outType, outputName);
  }

  // The text after the last dot in the output file name denotes
  // the extension and the filetype.
  unsigned int pos = static_cast<unsigned>(outputName.length());

  if (pos > 0)
    pos = static_cast<unsigned>(
        outputName.find_last_of('.', outputName.length()));

  if (pos < outputName.length()) { // concrete output type set
    string ext2("");

    ext2.assign(outputName, pos + 1, outputName.length() - pos - 1);
    env.SetString("OutputFileType", ext2.c_str());
  } else {
    WARNING << " type of the output file was not found\n";
    // no output type, it can cause the problem when saving application result
    env.SetString("OutputFileType", "");
  }

  return;
}

// reports the statistics result of the specific application
bool CSynthesisApp::DoReport(const CWorld & /*world*/, ostream &app) {
  // unconditional statistics
  app << "#---------------------------------------------------------\n";
  app << "#CSynthesisApp - REPORT\n";
  app << "#---------------------------------------------------------\n";

  // provide timing statistics
  app << "#--------------------------------------------------------------\n";
  app << "# Elapsed times and System resources for UNIX\n";
  app << "#--------------------------------------------------------------\n";
  app << _timer.RealTime() << "\n";

  // objects' count statistics
  if (_objects) {
    app << "#--------------------------------------------------------------\n";
    app << "# Misc. values - object counts\n";
    app << "#--------------------------------------------------------------\n";
    app << "#ObjectCount\n";
    app << _objects->size() << "\n";
  }
  return false; // everything correct
}

// Helper function that creates the application according to its name
// In case of failure returns NULL.
CSynthesisApp *CSynthesisApp::CreateApplication(const string &applicationName) {
  // default setting
  CSynthesisApp *application = NULL;

  if (applicationName.compare("RTCore") == 0)
    // just core of the ray tracer
    application = new CRT_Core_App();
// Exclude the application from the compilations
#ifndef __EXCLUDE_APPS
  else if ((applicationName.compare("RT00") == 0) ||
           (applicationName.compare("RT000") == 0))
    // classical recursive ray tracing
    application = new CRT_00_App();
  else if ((applicationName.compare("RTSSE") == 0) ||
           (applicationName.compare("RT01") == 0) ||
           (applicationName.compare("RT001") == 0))
    // recursive ray tracing with 2x2 packets and use of SSE instructions
    application = new CRT_SSE_App();
// Exclude the application from the compilations
#endif // __EXCLUDE_APPS

  return application;
}

__END_GOLEM_SOURCE

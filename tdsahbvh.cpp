// standard headers
#include <algorithm>
#include <cmath>
#include <cstdio>
#include <immintrin.h>
#include <ostream>

// nanogolem headers
#include "configg.h"
#include "errorg.h"
#include "hminfo.h"
#include "ray.h"
#include "tdsahbvh.h"
#include "vector3.h"
#include "world.h"

__BEGIN_GOLEM_SOURCE

// default constructor
CBVHSAH::CBVHSAH() : CTDBVH() { _splittingAlgorithm = 1; }

CTDBVH::SBVHnode *CBVHSAH::BuildRecursivelyWithSAH(int firstIndex,
                                                   int lastIndex) {
  int size = lastIndex - firstIndex;

  if (size <= 1) { // create a single leaf
    // create the leaf and finish
    SBVHnode *leaf = new SBVHnode;
    leaf->type = 0;
    assert(leaf);
    leaf->obj = boxedObjects[firstIndex].obj;
    return leaf;
  }

  if (size == 2) { // create two leaves
    // Sort the objects in X axis
    if (boxedObjects[firstIndex].bboxCentroid[0] >
        boxedObjects[firstIndex + 1].bboxCentroid[0]) {
      Swap(boxedObjects[firstIndex], boxedObjects[firstIndex + 1]);
    }

    // Create the BVH with exactly two primitives in both leaves
    SBVHnode *prim1 = new SBVHnode;
    assert(prim1);
    prim1->type = 0; // leaf
    prim1->obj = boxedObjects[firstIndex].obj;
    SBVHnode *prim2 = new SBVHnode;
    assert(prim2);
    prim1->type = 0; // leaf
    prim2->obj = boxedObjects[firstIndex + 1].obj;

    SBVHinterior *node = new SBVHinterior;
    assert(node);
    node->left = prim1;
    node->right = prim2;

    node->bbox = boxedObjects[firstIndex].bbox;
    node->bbox.Include(boxedObjects[firstIndex + 1].bbox);
    node->type = 1; // interior node
    // always use the X axis for the last order of interior nodes as it does not
    // matter which axis we choose
    return node;
  }

  int bestAxis = -1, bestSplit = -1;
  // Set to max value to always find a better value
  double bestCost = MAXFLOAT;
  double cost;

  SBBox tempBox, bbox;
  for (int axis = 0; axis < 3; axis++) {
    // sort objects by centroid of their bounding boxes in given axis
    cmp.dim = axis;
    std::sort(boxedObjects + firstIndex, boxedObjects + lastIndex, cmp);

    // sweep from right to left
    tempBox.Initialize();
    boxedObjects[lastIndex - 1].sizeRight = MAXFLOAT;
    for (int i = lastIndex - 2; i >= firstIndex; i--) {
      tempBox.Include(boxedObjects[i + 1].bbox);
      boxedObjects[i].sizeRight = tempBox.SA2() * 2;
    }

    // sweep from left to right
    tempBox.Initialize();
    for (int i = firstIndex, counter = 1; i < lastIndex; i++, counter++) {
      tempBox.Include(boxedObjects[i].bbox);
      boxedObjects[i].sizeLeft = tempBox.SA2() * 2;

      cost = ((boxedObjects[i].sizeLeft / rootSize) * counter) +
             ((boxedObjects[i].sizeRight / rootSize) * (size - counter + 1));
      if (cost < bestCost) {
        bestCost = cost;
        bestAxis = axis;
        bestSplit = i + 1;
      }

      // save bounding box of whole node
      if (axis == 0 && i == lastIndex - 1) {
        bbox = tempBox;
      }
    }
  }

  // re-sort in by best found axis
  cmp.dim = bestAxis;
  std::sort(boxedObjects + firstIndex, boxedObjects + lastIndex, cmp);

  // Setup node
  SBVHinterior *node = new SBVHinterior();
  node->bbox = bbox;
  node->type = bestAxis + 1;

  //---------------------------------------------------------
  // Create left subtree
  SBVHnode *leftSubtree = 0;
  if (bestSplit - firstIndex >= 1) {
    currentDepth++;
    leftSubtree = BuildRecursivelyWithSAH(firstIndex, bestSplit);
    currentDepth--;
  }

  //---------------------------------------------------------
  // Create right subtree
  SBVHnode *rightSubtree = 0;
  if (lastIndex - bestSplit >= 1) {
    currentDepth++;
    rightSubtree = BuildRecursivelyWithSAH(bestSplit, lastIndex);
    currentDepth--;
  }

  // Setup links to the children
  node->left = leftSubtree;
  node->right = rightSubtree;

  return node;
}

void CBVHSAH::BuildUp(const CObjectList &objlist) {

  root = 0;
  const int maxSize = static_cast<int>(objlist.size());
  // The array of boxes to be initialized
  boxedObjects = new SBBoxedObject3D[maxSize];
  assert(boxedObjects);

  // Initialize the box of the whole scene
  bbox.Initialize();

  // statistics data
  currentDepth = 0;
  int i = 0;

  // initialize boxed objects
  for (CObjectList::iterator it = ((CObjectList *)&objlist)->begin();
       it != objlist.end(); it++, i++) {
    boxedObjects[i].obj = *it;
    boxedObjects[i].bbox = boxedObjects[i].obj->GetBox();
    bbox.Include(boxedObjects[i].bbox);
    boxedObjects[i].bboxCentroid = boxedObjects[i].bbox.ComputeCentroid();
  }

  // Build recursively
  rootSize = bbox.SA2() * 2;
  root = BuildRecursivelyWithSAH(0, maxSize);

  delete[] boxedObjects;
  boxedObjects = 0;
  
  return;
}

void CBVHSAH::ProvideID(ostream &app) {
  app << "#CASDS = CBVHSAH - BVH top down, using SAH heuristic\n";
  app << (int)GetID() << "\n";
}

__END_GOLEM_SOURCE
// ============================================================================
//
// hminfo.h
//
// Class: CHitPointInfo
//            information about the intersection of a ray with 3D shape
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file
// doc/Licence.txt
//
// Initial coding by Vlastimil Havran, December 1999

#ifndef __HMINFO_H__
#define __HMINFO_H__

// standard headers

// nanoGOLEM headers
#include "configh.h"
#include "errorg.h"
#include "ray.h"
#include "vector3.h"

__BEGIN_GOLEM_HEADER

// forward declarations
class CObject3D;
class CColor;

// --------------------------------------------------------------------------
// This class includes information about intersection of the ray and the object
// it is used for computation MapPointInfo
// and for the intersection computation as well
// --------------------------------------------------------------------------
class CHitPointInfo {
  friend class CAObjectList;
  friend class CTDBVH;
  friend class CTriangle;
  friend class CTriangleNorm;
  friend class CTriangleInt;

protected: // data
  // the smallest signed distance for intersection computation from
  // the origin of the ray. It is modified by the intersection routines.
  float t;

  // the maximum allowed signed distance for HitAtAllI and HitAtAllTransparentI
  // ray intersections, which is used in the recursion etc. This may be changed
  // by ray traversal algorithm !!!
  float maxt;

  // reference to the object that is intersected, NULL if no object.
  const CObject3D *object;

  // if normal is valid (was computed by object) the value is true
  bool normalValid;
  // point of intersection of the object's surface, it is used for normal
  // computation as the input. The information about signed distance cannot
  // be used as input for normal computation ! Use Extrap() before computing
  // normal.
  CVector3D point;

  // the normal of object in point of intersection
  CVector3D normal;
  // this is the place to store the flags
  int flags;
  // reference to the ray for which the intersection was computed
  const CRay *ray;

  // this are texture space coordinates in u-v space (2D) for successfull
  // intersection, it can be set by (successfull only!)
  float u, v;

  // Barycentric coordinates for the closest intersection with respect 'maxt'
  float alpha, beta;

  // For texture antialiasing etc.
  // differential vector in direction u, perpendicular to normal.
  CVector3D diffU;
  // differential vector in direction v, perpendicular to normal.
  CVector3D diffV;

  int intersecCounter = 0;
  int steps = 0;
  // -----------------------------------------------------------------------
public: // member functions
  /// Constructor
  CHitPointInfo(const CObject3D *obj = NULL, const CRay *Ray = NULL)
      : object(obj), ray(Ray) {
    normalValid = false;
    point.Set(MAXFLOAT);
  }
  /// Copy constructor
  CHitPointInfo(const CHitPointInfo &info);

  /// Destructor
  ~CHitPointInfo() {}

  // assignment operator
  CHitPointInfo &operator=(const CHitPointInfo &info);

  // (re)initialize the info, when it should be used again for new ray,
  // intersection etc.
  void Init() {
    object = NULL;
    ray = NULL;
    normalValid = false;
    point.Set(MAXFLOAT);
  }

  // sets the info for FindNearestI call, ray must be passed and some data
  // must be reinitialized
  void SetForFindNearest(const CRay &newRay) {
    object = NULL;
    ray = &newRay;
    t = CLimits::Threshold;
    maxt = CLimits::Infinity;
    normalValid = false;
    point.Set(MAXFLOAT);
  }

  void Intersection() { intersecCounter++; }
  int GetIntersectionCounter() const { return intersecCounter; }
  void Step() { steps++; }
  int GetSteps() const { return steps; }

  // returns the normal (shading one, can be changed bump mapping)
  void GetNormal(CVector3D &rnormal) const { rnormal = GetNormal(); }
  CVector3D GetNormal();
  // returns the shading normal as well, faster access
  const CVector3D &GetNormal() const { return normal; }
  bool GetNormalValid() const { return normalValid; }
  // returns the signed distance for this info
  float GetT() const { return t; }

  // returns the maximum signed distance for traversal at current node
  float GetMaxT() const { return maxt; }

  // set/get u-v coordinates of the intersection for mapping purposes
  void SetUV(const float &uM, const float &vM) {
    u = uM;
    v = vM;
  }
  const float &GetU() const { return u; }
  const float &GetV() const { return v; }
  void GetUV(float &ru, float &rv) const {
    ru = u;
    rv = v;
  }

  void GetDifferentials(CVector3D &u, CVector3D &v) const;

  // this is one way how to set the tangent vectors to the normal
  void SetDifferentials(CVector3D &uVec, CVector3D &vVec) {
    diffU = uVec;
    diffV = vVec;
  }

  // -----------------------------------------------------------------------
  // return the point of intersection
  const CVector3D &GetPoint() const { return point; }
  // return the pointer to the associated ray
  const CRay *GetRay() const { return ray; }
  // return the object
  const CObject3D *GetObject3D() const { return object; }
  float GetAlpha() const { return alpha; }
  float GetBeta() const { return beta; }
  void SetAlpha(float _alpha) { alpha = _alpha; }
  void SetBeta(float _beta) { beta = _beta; }

  // computes the intersection point from a given signed distance
  void Extrap() {
    if (ray)
      point = ray->Extrap(t);
  }
  // -----------------------------------------------------------------------
  // the correction of normal and intersection point is needed
  // for bump and displacement mapping
  void SetNormal(const CVector3D &newNormal) { this->normal = newNormal; }
  void SetNormalValid(bool newNormalValid) {
    this->normalValid = newNormalValid;
  }
  void SetObject(const CObject3D *obj) { object = obj; }
  void SetT(float t) { this->t = t; }
  void SetMaxT(float _maxt) { maxt = _maxt; }

  // for debugging purposes
  friend ostream &operator<<(ostream &s, const CHitPointInfo &h);
};

__END_GOLEM_HEADER

#endif // __HMINFO_H__

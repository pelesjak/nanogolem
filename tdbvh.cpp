// ===================================================================
//
// tdbvh.cpp
//      CTBVH accelerating data structure for ray-shooting
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file
// doc/Licence.txt
//
// Initial coding by Vlastimil Havran, November 2005

// nanoGOLEM headers
#include "tdbvh.h"
#include "configg.h"
#include "hminfo.h"
#include "ray.h"
#include "vector3.h"
#include "world.h"

__BEGIN_GOLEM_SOURCE

// ------------------------------------------------------------
inline CTDBVH::SBVHinterior::SBVHinterior(SBVHnode *prim1, SBVHnode *prim2,
                                          const SBBox &_bbox) {
  bbox = _bbox;
  this->left = prim1;
  this->right = prim2;
}

void CTDBVH::SBVHinterior::Delete() {
  if (this->left->type == 0) {
    delete (SBVHnode *)this->left;
  } else {
    // Call recursively for left subtree
    ((SBVHinterior *)((SBVHinterior *)this)->left)->Delete();
    delete ((SBVHinterior *)this)->left;
  }

  // --------------------------------
  if (this->right->type == 0) {
    delete (SBVHnode *)this->right;
  } else {
    // Call recursively for right subtree
    ((SBVHinterior *)((SBVHinterior *)this)->right)->Delete();
    delete ((SBVHinterior *)this)->right;
  }
}

// ----------------------------------------------------------
// class CTDBVH

// The stack for traversal, static variable
CTDBVH::SStackEntry CTDBVH::stack[CTDBVH::DepthOfStack];

// default constructor
CTDBVH::CTDBVH() {
  root = 0;
  array = 0;
#ifdef ARRAY_BOXES
  boxes = 0;
#endif // ARRAY_BOXES
}

// Delete the data structures
void CTDBVH::Remove() {
  if (root) {
    ((SBVHinterior *)root)->Delete();
    delete root;
    root = 0;
  }
}

// default destructor
CTDBVH::~CTDBVH() { Remove(); }

void CTDBVH::BuildUp(const CObjectList &objlist) {
  root = 0;
  const int maxSize = static_cast<int>(objlist.size());
  array = new CObject3D *[maxSize];
  assert(array);
  // The array of boxes to be initialized
#ifdef ARRAY_BOXES
  boxes = new SBBox[maxSize];
  assert(boxes);
#endif // ARRAY_BOXES

  // Initialize the box of the whole scene
  bbox.Initialize();

  // Initial box of the whole scene
  InitializeBox(this->bbox, (CObjectList &)objlist);

  // statistics data
  currentDepth = 0;

  // The object for individual object
  SBBox objbox;
  int i = 0;
  countObjects = 0;

  // copy to the array
  for (CObjectList::iterator it = ((CObjectList *)&objlist)->begin();
       it != objlist.end(); it++, i++) {
    CObject3D *obj = *it;
    objbox = obj->GetBox();
    bbox.Include(objbox);
    array[countObjects] = (CObject3D *)obj;
    // The array of boxes to be initialized
#ifdef ARRAY_BOXES
    boxes[countObjects] = objbox;
#endif // ARRAY_BOXES

    // increase the number of objects - correct counter value
    countObjects++;
  } // for

  assert(countObjects <= maxSize);

  // Pickup the longest side first
  int axis = bbox.Diagonal().DrivingAxis();

  // Try to compute the cost estimate using a new method
  float SAsum = 0.f;
  for (int i = 0; i < countObjects; i++) {
#ifdef ARRAY_BOXES
    SBBox bo = boxes[i];
#else
    CAABox btt = array[countObjects]->BBox();
    SBBox bo(btt.Min(), btt.Max());
#endif // ARRAY_BOXES
    SAsum += 2.0f * bo.SA2();
  } // for i

  // Build recursively
  // Longest axis selection algorithm + round robin next
  root = BuildRecursively(bbox, 0, countObjects, axis);

#ifdef ARRAY_BOXES
  delete[] boxes;
  boxes = 0;
#endif // ARRAY_BOXES

  delete[] array;
  array = 0;

  return;
}

void CTDBVH::CheckRecursively(SBVHnode *node) const {
  if (node->type == 0) {
    //// leaf node
    // STATS << "tri, node=" << (void*)(node);
    // STATS << "  node->obj=" << (void*)(node->obj);
    // STATS << "  node->obj->ID=" << node->obj->ID();
    return;
  }
  assert(node->type > 0);

  // interior node
  SBVHnode *left = (SBVHinterior *)node->left;
  SBVHnode *right = ((SBVHinterior *)node)->right;

  CheckRecursively(left);
  CheckRecursively(right);
  return;
}

// --------------------------------------------------------------
// Quicksort split - using bubble sort technique
unsigned int Qsplit(unsigned int firstIndex, unsigned int lastIndex,
                    SBBox boxes[], CObject3D **array, float splitValue,
                    unsigned int axis,
                    // output values
                    SBBox &bboxLeft, SBBox &bboxRight) {
  bboxLeft.Initialize();
  bboxRight.Initialize();

  float centroid;
  unsigned int ret_val = firstIndex;

#ifdef ARRAY_BOXES
  SBBox tmpbox;
  SBBox obbox;
#else  // ARRAY_BOXES
  CAABox obbox;
#endif // ARRAY_BOXES

  // This is overly complicated and costly sorting !!!
  // It can be done more efficiently
  for (unsigned int i = firstIndex; i < lastIndex; i++) {
#ifdef ARRAY_BOXES
    obbox = boxes[i];
    // Compute the centroid of the object
    centroid = (obbox.Min()[axis] + obbox.Max()[axis]) * 0.5f;
#else  // ARRAY_BOXES
    obbox = array[i]->GetBox();
    // Compute the centroid of the object
    centroid = (obbox.Min()(axis) + obbox.Max()(axis)) * 0.5f;
#endif // ARRAY_BOXES

    if (centroid < splitValue) {
      // put the object to the left
      CObject3D *temp = array[i];
      array[i] = array[ret_val];
      array[ret_val] = temp;

#ifdef ARRAY_BOXES
      tmpbox = boxes[i];
      boxes[i] = boxes[ret_val];
      boxes[ret_val] = tmpbox;
#endif // ARRAY_BOXES

      bboxLeft.Include(obbox);
      ret_val++; // the number of objects in the left
    } else {
      // Compute new right bounding box
      bboxRight.Include(obbox);
    }
  } // for i

  // Check if the case is not problematic
  if ((ret_val == firstIndex) || (ret_val == lastIndex)) {
    ret_val = (firstIndex + lastIndex) / 2;
    // In this case we have to reinitialize the bounding boxes
    bboxLeft.Initialize();
    bboxRight.Initialize();

    unsigned int i;
    for (i = firstIndex; i < ret_val; i++) {
#ifdef ARRAY_BOXES
      bboxLeft.Include(boxes[i]);
#else  // ARRAY_BOXES
      obbox = array[i]->GetBox();
      bboxLeft.Include(obbox);
#endif // ARRAY_BOXES
    }  // for i

    for (i = ret_val; i < lastIndex; i++) {
#ifdef ARRAY_BOXES
      bboxRight.Include(boxes[i]);
#else  // ARRAY_BOXES
      obbox = array[i]->GetBox();
      bboxRight.Include(obbox);
#endif // ARRAY_BOXES
    }  // for i
  }    // if

#ifdef _DEBUG
  if (bboxLeft.MinX() == MAXFLOAT) {
    DEBUG << "Left box is wrong" << endl;
  }
  if (bboxRight.MinX() == MAXFLOAT) {
    DEBUG << "Right box is wrong" << endl;
  }
#endif // _DEBUG

  // Return the index to be taken
  return ret_val;
}

// The function for building recursively
CTDBVH::SBVHnode *CTDBVH::BuildRecursively(const SBBox &box,
                                           unsigned int firstIndex,
                                           unsigned int lastIndex,
                                           unsigned int axis) {
  unsigned int size = lastIndex - firstIndex;

  if (size <= 1) { // create a single leaf
    // create the leaf and finish
    SBVHnode *leaf = new SBVHnode;
    leaf->type = 0;
    assert(leaf);
    leaf->obj = array[firstIndex];
    // leaf->type = 0;
    return leaf;
  }

  // ------------------------------------------
  if (size == 2) { // create two leaves
#ifdef ARRAY_BOXES
    // Compute boxes of the objects
    if (boxes[firstIndex].pp[0][axis] + boxes[firstIndex].pp[1][axis] >
        boxes[firstIndex + 1].pp[0][axis] + boxes[firstIndex + 1].pp[1][axis]) {
      // ---------------------------------------
      Swap(array[firstIndex], array[firstIndex + 1]);
      Swap(boxes[firstIndex], boxes[firstIndex + 1]);
      // Now the left object is going to be the left child
      // and the right object the right child
    }
#else  // ARRAY_BOXES
    // Compute boxes of the objects
    CAABox lbbox = array[firstIndex]->GetBox();
    CAABox rbbox = array[firstIndex + 1]->GetBox();
    if (lbbox.Min(axis) + lbbox.Max(axis) > rbbox.Min(axis) + rbbox.Max(axis)) {
      // ---------------------------------------
      Swap(array[firstIndex], array[firstIndex + 1]);
      // Now the left object is going to be the left child
      // and the right object the right child
    }
#endif // ARRAY_BOXES

    // Create the BVH with exactly two primitives in both leaves
    SBVHnode *prim1 = new SBVHnode;
    assert(prim1); // leaf
    prim1->type = 0;
    prim1->obj = array[firstIndex];
    SBVHnode *prim2 = new SBVHnode;
    assert(prim2);
    prim1->type = 0; // leaf
    prim2->obj = array[firstIndex + 1];
    SBVHinterior *node = new SBVHinterior;
    assert(node);
    node->left = prim1;
    node->bbox = box;
    node->right = prim2;
    node->type = 1 + axis; // interior node + axis information
    return node;
  }

  // Computation of the split results, sort objects into
  // two parts
  unsigned int midpoint;
  SBBox bboxLeft, bboxRight;

  // -------------------------------------------------------------------
  // interior node to be split at this point, compute
  // spatial median given the axis
  float splitValue = (box.pp[0][axis] + box.pp[1][axis]) * 0.5f;

  // Create the list of left and right objects
  midpoint = Qsplit(firstIndex, lastIndex, boxes, array, splitValue, axis,
                    // output values
                    bboxLeft, bboxRight);
  assert(axis < 3);

  // Setup node
  SBVHinterior *node = new SBVHinterior();
  node->bbox = box;
  // This is here changed for interior node
  // X=1, Y=2, Z=3. We try to optimize traversal order
  node->type = axis + 1;

  //---------------------------------------------------------
  // Create left subtree
  // int axisLeft = bboxLeft.Diagonal().DrivingAxis();
  unsigned int axisLeft = (axis + 1) % 3;
  SBVHnode *leftSubtree = 0;
  if (midpoint - firstIndex >= 1) {
    currentDepth++;
    leftSubtree = BuildRecursively(bboxLeft, firstIndex, midpoint, axisLeft);
    currentDepth--;
  }

  //---------------------------------------------------------
  // Create right subtree
  // int axisRight = bboxRight.Diagonal().DrivingAxis();
  unsigned int axisRight = (axis + 1) % 3;
  SBVHnode *rightSubtree = 0;
  if (lastIndex - midpoint >= 1) {
    currentDepth++;
    rightSubtree = BuildRecursively(bboxRight, midpoint, lastIndex, axisRight);
    currentDepth--;
  }

  // Setup links to the children
  node->left = leftSubtree;
  node->right = rightSubtree;

  return node;
}

// Optimized version of the code, traversing in the right order
// based on the direction of the ray
const CObject3D *CTDBVH::FindNearestI(CRay &ray, CHitPointInfo &info) {
  float tmin = 0; //-CLimits::Infinity;
  float tmax = CLimits::Infinity;

  // capture number of intersection calculations
  info.Intersection();
  // test if the whole CKDR tree is missed by the input ray
  if ((!bbox.RayIntersect(ray, tmin, tmax)) || (!root)) {
    // no object can be intersected
    return (CObject3D *)NULL;
  }
  // capture steps through BVH
  info.Step();
  // We intersect the box
  info.SetMaxT(Min(info.GetMaxT(), tmax));

  // -------------------------------------------
  // This is the definition of the stack
  static CTDBVH::SStackEntry stack[100];
  int indexStack = 1;

  // start from the root node
  SBVHnode *node = root;
  // save maximum signed distance
  float saveMaxT = info.GetMaxT();
  const CVector3D &rdir = ray.GetDir();
  // int rayID = ray.GetID();
  // int a = 0;

  // -------------------------------------------
  while (indexStack > 0) {
    // Descend in the hierarchy
    while (node) {
      if (node->type == 0) {
        // capture number of intersection calculations
        info.Intersection();
        // this is leaf with one object
        if (node->obj->NearestInt(ray, info)) {
          // object was surely hit, restrict the maximum distance
          info.SetMaxT(info.GetT());
        }
        break; // the pop from the stack is needed
      } else {
        // capture number of intersection calculations
        info.Intersection();
        // an internal node
        if (((SBVHinterior *)node)->bbox.RayIntersect(ray, tmin, tmax)) {
          // capture steps through BVH
          info.Step();
          // The box was hit
          SBVHnode *nearSubtree, *farSubtree;
          if (rdir[(node->type - 1)] > 0) {
            nearSubtree = (SBVHinterior *)node->left;
            farSubtree = ((SBVHinterior *)node)->right;
          } else {
            nearSubtree = ((SBVHinterior *)node)->right;
            farSubtree = (SBVHinterior *)node->left;
          }

          if (farSubtree) {
            // push the far child to the stack
            stack[indexStack].pointer = farSubtree;
            stack[indexStack].mint = tmin;
            stack[indexStack].maxt = tmax;
            indexStack++;
          }

          if (nearSubtree)
            // go to the near child
            node = nearSubtree;
          else
            break; // pop the node from the stack
        }          // if
        else
          break; // we should also pop in this case
      }          // if internal node or a leaf
    }            // while descending

    // pop a node from the stack
    indexStack--;
    tmin = stack[indexStack].mint;
    tmax = stack[indexStack].maxt;
    node = stack[indexStack].pointer;
    // If some object was already intersected
    if (info.GetObject3D()) {
      if (tmin > info.GetMaxT()) {
        // we can pop another node from the stack,
        // since the node just popped is irrelevant
        while (indexStack > 0) {
          indexStack--;
          tmin = stack[indexStack].mint;
          if (tmin < info.GetMaxT())
            break; // this node can be still relevant
        }          // while
        node = stack[indexStack].pointer;
        tmax = stack[indexStack].maxt;
      } // if the extent is irrelevant
    }   // if some object was already intersected
  }     // while indexStack

  // recover maximum signed distance
  info.SetT(info.GetMaxT());
  info.SetMaxT(saveMaxT);

  return (CObject3D *)info.GetObject3D();
}

unsigned CTDBVH::FindNearestI(CRayPacket &rayPacket,
                              CRPHitPointsInfo &packetInfo) {
  // STATUS << "BVHSAH FindNearest of ray packet" << endl;

  __m128 tmin = _mm_set1_ps(CLimits::Threshold); //-CLimits::Infinity;
  __m128 tmax = _mm_set1_ps(CLimits::Infinity);

  // capture number of intersection calculations
  packetInfo.Intersection();
  // test if the whole CKDR tree is missed by the input ray
  if ((!bbox.RayPacketIntersect(rayPacket, tmin, tmax)) || (!root)) {
    // no object can be intersected
    return 0;
  }
  // capture steps through BVH
  packetInfo.Step();
  // We intersect the box
  packetInfo.maxt = _mm_min_ps(packetInfo.maxt, tmax);

  // -------------------------------------------
  // This is the definition of the stack
  static CTDBVH::SStackEntrySSE stack[100];
  int indexStack = 1;

  // start from the root node
  SBVHnode *node = root;
  // save maximum signed distance
  __m128 saveMaxT = packetInfo.maxt;
  const CVector3D &rdir = rayPacket.avgDir;
  // int rayID = ray.GetID();
  // int a = 0;

  // -------------------------------------------
  while (indexStack > 0) {
    // Descend in the hierarchy
    while (node) {
      if (node->type == 0) {
        // capture number of intersection calculations
        packetInfo.Intersection();
        // this is leaf with one object
        node->obj->NearestInt(rayPacket, packetInfo);
        break; // the pop from the stack is needed
      } else {
        // an internal node
        packetInfo.Intersection();
        if (((SBVHinterior *)node)
                ->bbox.RayPacketIntersect(rayPacket, tmin, tmax)) {
          // capture steps through BVH
          packetInfo.Step();

          // The box was hit
          SBVHnode *nearSubtree, *farSubtree;
          if (rdir[(node->type - 1)] > 0) {
            nearSubtree = (SBVHinterior *)node->left;
            farSubtree = ((SBVHinterior *)node)->right;
          } else {
            nearSubtree = ((SBVHinterior *)node)->right;
            farSubtree = (SBVHinterior *)node->left;
          }

          if (farSubtree) {
            // push the far child to the stack
            stack[indexStack].pointer = farSubtree;
            stack[indexStack].mint = tmin;
            stack[indexStack].maxt = tmax;
            indexStack++;
          }

          if (nearSubtree)
            // go to the near child
            node = nearSubtree;
          else
            break; // pop the node from the stack
        }          // if
        else
          break; // we should also pop in this case
      }          // if internal node or a leaf
    }            // while descending

    // pop a node from the stack
    indexStack--;
    tmin = stack[indexStack].mint;
    tmax = stack[indexStack].maxt;
    node = stack[indexStack].pointer;
    // If some object was already intersected with every ray in packet
    if (packetInfo.intersected) {
      // if(tmin > info.maxt) if this condition is true for all rays
      if (_mm_movemask_ps(_mm_cmpgt_ps(tmin, packetInfo.maxt)) ==
          _MM_MASK_TRUE) {
        // we can pop another node from the stack,
        // since the node just popped is irrelevant
        while (indexStack > 0) {
          indexStack--;
          tmin = stack[indexStack].mint;
          // if (tmin < packetInfo.maxt) break if this true for any of the rays
          if (_mm_movemask_ps(_mm_cmplt_ps(tmin, packetInfo.maxt)))
            break; // this node can be still relevant
        }          // while
        node = stack[indexStack].pointer;
        tmax = stack[indexStack].maxt;
      } // if the extent is irrelevant
    }   // if some object was already intersected
  }     // while indexStack

  // recover maximum signed distance
  packetInfo.t = packetInfo.maxt;
  packetInfo.maxt = saveMaxT;

  return packetInfo.intersected;
}

void CTDBVH::ProvideID(ostream &app) {
  app << "#CASDS = CTDBVH - BVH top down, Kajiya86 paper\n";
  app << (int)GetID() << "\n";
}

__END_GOLEM_SOURCE

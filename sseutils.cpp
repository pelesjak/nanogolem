#include "sseutils.h"
#include "configg.h"
#include "hminfo.h"
#include "triangle.h"
#include <immintrin.h>

__BEGIN_GOLEM_SOURCE

ufloat __FLOAT_SIGN_MASK = {0x7FFFFFFF};

__m128 _MM_ABS_MASK = _mm_set1_ps(__FLOAT_SIGN_MASK.f);

//*******************************************
// SSE helper functions
//*******************************************
__m128 _mm_abs_ps(__m128 value) { return _mm_and_ps(_MM_ABS_MASK, value); };

__m128 _mm_cndval_ps(__m128 &condition, __m128 pos, __m128 neg) {
  return _mm_or_ps(_mm_and_ps(condition, pos), _mm_andnot_ps(condition, neg));
}

void _mm_cndswap_ps(__m128 &condition, __m128 &a, __m128 &b) {
  __m128 temp = a;
  a = _mm_cndval_ps(condition, b, a);
  b = _mm_cndval_ps(condition, temp, b);
}

//*******************************************
// __mCVector3D functions
//*******************************************
void CVectorSSE3D::Normalize() {
  __m128 x2 = _mm_mul_ps(x, x);
  __m128 y2 = _mm_mul_ps(y, y);
  __m128 z2 = _mm_mul_ps(z, z);

  __m128 sum = _mm_add_ps(x2, y2);
  sum = _mm_add_ps(sum, z2);

  __m128 size = _mm_sqrt_ps(sum);

  x = _mm_div_ps(x, size);
  y = _mm_div_ps(y, size);
  z = _mm_div_ps(z, size);
}

CVectorSSE3D CVectorSSE3D::CrossProd(CVector3D &vec) {
  __m128 vecX = _mm_set1_ps(vec.x), vecY = _mm_set1_ps(vec.y),
         vecZ = _mm_set1_ps(vec.z);
  __m128 cx = _mm_sub_ps(_mm_mul_ps(y, vecZ), _mm_mul_ps(z, vecY));
  __m128 cy = _mm_sub_ps(_mm_mul_ps(z, vecX), _mm_mul_ps(x, vecZ));
  __m128 cz = _mm_sub_ps(_mm_mul_ps(x, vecY), _mm_mul_ps(y, vecX));
  return CVectorSSE3D(cx, cy, cz);
}

__m128 CVectorSSE3D::DotProd(CVector3D &vec) {
  __m128 vecX = _mm_set1_ps(vec.x), vecY = _mm_set1_ps(vec.y),
         vecZ = _mm_set1_ps(vec.z);
  return _mm_add_ps(_mm_add_ps(_mm_mul_ps(x, vecX), _mm_mul_ps(y, vecY)),
                    _mm_mul_ps(z, vecZ));
}

//*******************************************
// CRPHitPointsInfo functions
//*******************************************
void CRPHitPointsInfo::Init(CRayPacket *rp) {
  t = _mm_set1_ps(CLimits::Threshold);
  maxt = _mm_set1_ps(CLimits::Infinity);

  rayPacket = rp;
  intersected = 0;

  objects[0] = nullptr;
  objects[1] = nullptr;
  objects[2] = nullptr;
  objects[3] = nullptr;
}

void CRPHitPointsInfo::Extrap() {
  intersections.x =
      _mm_add_ps(rayPacket->origin.x, _mm_mul_ps(t, rayPacket->dir.x));
  intersections.y =
      _mm_add_ps(rayPacket->origin.y, _mm_mul_ps(t, rayPacket->dir.y));
  intersections.z =
      _mm_add_ps(rayPacket->origin.z, _mm_mul_ps(t, rayPacket->dir.z));
}

void CRPHitPointsInfo::GetNormals() {
  float alphas[4] __attribute__((aligned(16)));
  float betas[4] __attribute__((aligned(16)));
  _mm_store_ps(alphas, alpha);
  _mm_store_ps(betas, beta);
  for (int i = 0; i < 4; i++) {
    if (objects[i] != nullptr) {
      normalsSeq[i] = objects[i]->FindNormal(alphas[i], betas[i]);
    }
  }
  normals.x = _mm_set_ps(normalsSeq[3].x, normalsSeq[2].x, normalsSeq[1].x,
                         normalsSeq[0].x);
  normals.y = _mm_set_ps(normalsSeq[3].y, normalsSeq[2].y, normalsSeq[1].y,
                         normalsSeq[0].y);
  normals.z = _mm_set_ps(normalsSeq[3].z, normalsSeq[2].z, normalsSeq[1].z,
                         normalsSeq[0].z);
}

CVectorSSE3D CrossProd(CVectorSSE3D &a, CVectorSSE3D &b) {
  __m128 cx = _mm_sub_ps(_mm_mul_ps(a.y, b.z), _mm_mul_ps(a.z, b.y));
  __m128 cy = _mm_sub_ps(_mm_mul_ps(a.z, b.x), _mm_mul_ps(a.x, b.z));
  __m128 cz = _mm_sub_ps(_mm_mul_ps(a.x, b.y), _mm_mul_ps(a.y, b.x));
  return CVectorSSE3D(cx, cy, cz);
}

__m128 DotProd(CVectorSSE3D &a, CVectorSSE3D &b) {
  return _mm_add_ps(_mm_add_ps(_mm_mul_ps(a.x, b.x), _mm_mul_ps(a.y, b.y)),
                    _mm_mul_ps(a.z, b.z));
}

__END_GOLEM_SOURCE

#ifndef __SSEUTILS_H__
#define __SSEUTILS_H__

// nanogolem headers
#include "configh.h"
#include "vector3.h"

// standard headers
#include <immintrin.h>

__BEGIN_GOLEM_HEADER

//*******************************************
// SSE constatns
//*******************************************
#define _MM_MASK_TRUE 0xFU

const __m128 _MM_ZERO = _mm_setzero_ps();
const __m128 _MM_MINUSONE = _mm_set1_ps(-1.0);

//*******************************************
// SSE helper functions
//*******************************************
__m128 _mm_abs_ps(__m128 value);
__m128 _mm_cndval_ps(__m128 &condition, __m128 pos, __m128 neg);
void _mm_cndswap_ps(__m128 &condition, __m128 &a, __m128 &b);

//*******************************************
// SSE data structures
//*******************************************
typedef float float_aligned16 __attribute__((aligned));

union ufloat {
  unsigned u;
  float f;
};

class CVectorSSE3D {
public:
  __m128 x;
  __m128 y;
  __m128 z;

  // Constructors
  CVectorSSE3D(){};
  CVectorSSE3D(float &X) { x = y = z = _mm_set1_ps(X); };
  CVectorSSE3D(__m128 &X) { x = y = z = X; };
  CVectorSSE3D(CVector3D vec) {
    x = _mm_set1_ps(vec.x);
    y = _mm_set1_ps(vec.y);
    z = _mm_set1_ps(vec.z);
  };
  CVectorSSE3D(float X, float Y, float Z) {
    x = _mm_set1_ps(X);
    y = _mm_set1_ps(Y);
    z = _mm_set1_ps(Z);
  };
  CVectorSSE3D(__m128 X, __m128 Y, __m128 Z) {
    x = X;
    y = Y;
    z = Z;
  };

  // public functions
  void Normalize();
  CVectorSSE3D CrossProd(CVector3D &vec);
  __m128 DotProd(CVector3D &vec);

  // operators
  friend inline CVectorSSE3D operator-(const CVectorSSE3D &A) {
    return CVectorSSE3D(_mm_mul_ps(A.x, _MM_MINUSONE),
                        _mm_mul_ps(A.y, _MM_MINUSONE),
                        _mm_mul_ps(A.z, _MM_MINUSONE));
  }
  friend inline CVectorSSE3D operator+(const CVectorSSE3D &A,
                                       const CVectorSSE3D &B) {
    return CVectorSSE3D(_mm_add_ps(A.x, B.x), _mm_add_ps(A.y, B.y),
                        _mm_add_ps(A.z, B.z));
  }
  friend inline CVectorSSE3D operator-(const CVectorSSE3D &A,
                                       const CVectorSSE3D &B) {
    return CVectorSSE3D(_mm_sub_ps(A.x, B.x), _mm_sub_ps(A.y, B.y),
                        _mm_sub_ps(A.z, B.z));
  }
  friend inline CVectorSSE3D operator*(const CVectorSSE3D &A, const __m128 &b) {
    return CVectorSSE3D(_mm_mul_ps(A.x, b), _mm_mul_ps(A.y, b),
                        _mm_mul_ps(A.z, b));
  }
};

class CColorSSE : public CVectorSSE3D {
public:
  // Constructors
  CColorSSE() : CVectorSSE3D(){};
  CColorSSE(float &X) : CVectorSSE3D(X){};
  CColorSSE(__m128 &X) : CVectorSSE3D(X){};
  CColorSSE(float &X, float &Y, float &Z) : CVectorSSE3D(X, Y, Z){};
  CColorSSE(__m128 &X, __m128 &Y, __m128 &Z) {
    x = X;
    y = Y;
    z = Z;
  };
  CColorSSE(__m128 X, __m128 Y, __m128 Z) {
    x = X;
    y = Y;
    z = Z;
  };
};

class CRayPacket {
public:
  // contructors
  CRayPacket() {}
  CRayPacket(CVector3D &o) { origin = o; }
  CRayPacket(CVector3D &o, CVectorSSE3D &d) {
    origin = o;
    dir = d;
  }
  CRayPacket(CVectorSSE3D &o, CVectorSSE3D &d) {
    origin = o;
    dir = d;
  }

  CVectorSSE3D origin;
  CVectorSSE3D dir;
  CVector3D avgDir;
};

// forward declarations
class CObject3D;

class CRPHitPointsInfo {
protected:
  // statistical data
  int intersecCounter = 0;
  int steps = 0;

public:
  __m128 t;
  __m128 maxt;

  __m128 alpha;
  __m128 beta;

  unsigned intersected;

  const CObject3D *objects[4];

  CVectorSSE3D intersections;

  CVectorSSE3D normals;
  CVector3D normalsSeq[4];

  CRayPacket *rayPacket;

  void Init(CRayPacket *rp);
  void Extrap();
  void GetNormals();

  // statistical data
  void Intersection() { intersecCounter++; }
  int GetIntersectionCounter() const { return intersecCounter; }
  void Step() { steps++; }
  int GetSteps() const { return steps; }
};

CVectorSSE3D CrossProd(CVectorSSE3D &a, CVectorSSE3D &b);
__m128 DotProd(CVectorSSE3D &a, CVectorSSE3D &b);

__END_GOLEM_HEADER

#endif // __SSEUTILS_H__

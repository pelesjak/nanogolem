############################################################################
# Written by Vlastimil Havran, 1998.					   #
#									   #
# This is the template file for utility tmake to generate Makefile	   #
# for GOLEM project, the library		                           #
# For information about tmake see   http://tmake.sourceforge.net/          #
# Run "tmake src.pro > Makefile"					   #
#									   #
# For MSVC run tmake -t vcapp -o golemall.dsp golemall.pro                 #
############################################################################

PROJECT		= golem
TEMPLATE	= app

# The configuration of the result
CONFIG		= warn_on release

#####################################################
# Select one from the options
#CONFIG		+= warn_on
#CONFIG		+= warn_off
#CONFIG		+= debug
#CONFIG		+= release

#####################################################
# Specific options
#SPECOPTIONS	= _DEBUG __TRAVERSAL_STATISTICS
#SPECOPTIONS	= _DEBUG

unix:SPECOPTIONS  += __UNIX__

# Select them from the possibilities below


#####################################################
# Do not change the settings below if possible
#####################################################
TARGET		= nanogolem
VERSION		= 1.0.0
DESTDIR		= ./bin
DEPENDPATH 	= ./
INCLUDEPATH	= ./
INCLUDE_H	= ./
SRC_H		= ./
OBJECTS_DIR	= ./objs
CLEAN_FILES	= core *~ $$INCLUDE_H/*~ $$OBJECTS_DIR/*.o $$SRC_H/*~ $$SRC_H/*/*~ $$SRC_H/*/*/*~

# The character which serves as DIR separator in file path
DIRCAT		= 0x2f
#DIRCAT		= \'/\'
#DIRCAT	= 92

LIBS	 	= -lm 

DEFINES = DIRCAT=$$DIRCAT
DEFINES += $$SPECOPTIONS
DEFINES	+= COMPILE_OPTIONS=$$COMPOPTIONS

# Definition for Unix
unix:LIBEXT		= .a
#win32-g++:LIBEXT	= .a
#win32-msvc:LIBEXT	= .lib

#ONLY IF __SDL_DISPLAY_SUPPORT is used
LIBS            += -lpng

#####################################################
# Add possibly a new header file here

HEADERS = $$INCLUDE_H/appcore.h \
          $$INCLUDE_H/assimpparse.h \
          $$INCLUDE_H/asds.h \
          $$INCLUDE_H/basic.h \
          $$INCLUDE_H/basmacr.h \
          $$INCLUDE_H/basmath.h \
          $$INCLUDE_H/basrnd.h \
          $$INCLUDE_H/basstr.h \
          $$INCLUDE_H/colorfs.h \
          $$INCLUDE_H/color.h \
          $$INCLUDE_H/coords3d.h \
          $$INCLUDE_H/configg.h \
          $$INCLUDE_H/configh.h \
          $$INCLUDE_H/environ.h \
          $$INCLUDE_H/errorg.h \
          $$INCLUDE_H/hdrimg.h \
          $$INCLUDE_H/hminfo.h \
          $$INCLUDE_H/light.h \
          $$INCLUDE_H/matrix4.h \
          $$INCLUDE_H/ray.h \
          $$INCLUDE_H/rt00app.h \
          $$INCLUDE_H/rtsseapp.h \
          $$INCLUDE_H/rtcoreca.h \
          $$INCLUDE_H/rtcore.h \
          $$INCLUDE_H/sbbox.h \
          $$INCLUDE_H/scene.h \
          $$INCLUDE_H/sseutils.h \
          $$INCLUDE_H/talloc.h \
          $$INCLUDE_H/tdbvh.h \
          $$INCLUDE_H/tdsahbvh.h \
          $$INCLUDE_H/tgaimg.h \
          $$INCLUDE_H/timer.h \
          $$INCLUDE_H/triangle.h \
          $$INCLUDE_H/vector3.h \
          $$INCLUDE_H/viewpars.h \
          $$INCLUDE_H/world.h

#####################################################
# Add possibly a new source file here

SOURCES = $$SRC_H/appcore.cpp \
          $$SRC_H/asds.cpp \
          $$INCLUDE_H/assimpparse.cpp \
          $$SRC_H/basrnd.cpp \
          $$SRC_H/basstr.cpp \
          $$SRC_H/color.cpp \
          $$SRC_H/coords3d.cpp \
          $$SRC_H/dotcount.cpp \
          $$SRC_H/environ.cpp \
          $$SRC_H/errorg.cpp \
          $$SRC_H/hdrimg.cpp \
          $$SRC_H/hdrio.cpp \
          $$SRC_H/hminfo.cpp \
          $$SRC_H/light.cpp \
          $$SRC_H/main.cpp \
          $$SRC_H/matrix4.cpp \
          $$SRC_H/ray.cpp \
          $$SRC_H/rt00app.cpp \
          $$SRC_H/rtsseapp.cpp \
          $$SRC_H/rtcoreca.cpp \
          $$SRC_H/rtcore.cpp \
          $$SRC_H/sbbox.cpp \
          $$SRC_H/scene.cpp \
          $$SRC_H/sseutils.cpp \
          $$SRC_H/talloc.cpp \
          $$SRC_H/tdbvh.cpp \
          $$SRC_H/tdsahbvh.cpp \
          $$SRC_H/timer.cpp \
          $$SRC_H/triangle.cpp \
          $$SRC_H/vector3.cpp \
          $$SRC_H/viewpars.cpp \
          $$SRC_H/world.cpp



// ================================================================
//
// hdrimg.h
//     This is the support to report messages, errors,
//       and to debug bugs in nanoGOLEM.
//
// Class: CHDRImage, CEnvironmentMap
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, February 2000

#ifndef __HDRIMG_H__
#define __HDRIMG_H__

// standard headers
#include <cassert>

using namespace std;

// nanoGOLEM headers
#include "configh.h"
#include "basrnd.h"
#include "color.h"
#include "coords3d.h"
#include "vector3.h"

__BEGIN_GOLEM_HEADER

enum EMarkType
{
  EE_BRDFSamples = 0,
  EE_ENVMAPSamples = 1,  
};

// This is the class for HDR image
class CHDRImage
{
protected:
  int width, height;
  // each pixel takes 3 float32, each component can be of any value...
  float *cols;
  string filenameRead; // if read, then filename
  string gname; // the name without extension and path
public:
  CHDRImage():width(0), height(0), cols(0) { }
  CHDRImage(const CHDRImage &src); // copy constructor
  // copy constructor with possibly making bargraph on the right
  CHDRImage(const CHDRImage &src, int pseudoColorTechnique,
	    bool copyImage = true, int widthBargraph = 30, int borderBargraph = 5);
  virtual ~CHDRImage() { delete []cols;}

  // load/save the data from HDR and PNG format
  // returns false on error
  bool LoadHDR(const string &fileName);
  bool LoadPNG(const string &fileName);
  bool SaveHDR(const string &fileName, bool verbose = false);
  bool SavePNG(const string &fileName);
  void CopyDataFromArray(int nwidth, int nheight, int channels,
			 float *buffer, int bufferWidth,
			 int xoffset, int yoffset,
			 bool flipVertically=false);
  virtual void Allocate(int nwidth, int nheight) {
    delete []cols;
    width = nwidth;
    height = nheight;
    int nmax = 3 * height * width;
    cols = new float[nmax];
    assert(cols);
    for (int i = 0; i < nmax; i++) cols[i] = 0;
  }
  int GetWidth() const { return width;}
  int GetHeight() const { return height;}
  
  const string GetName() const {return gname;}
  const string GetFileName() const {return filenameRead;}
  
  // get value RGB for a particular theta and phi, when we assume
  // that the HDR image is the environment map
  void SetValueChannel(int x, int y, int isp, float value) {
    assert((x >= 0)&&(x < width));
    assert((y >= 0)&&(y < height));
    cols[(width * y + x)*3 + isp] = value;
  }
  // get value RGB for a particular theta and phi, when we assume
  // that the HDR image is the environment map
  float GetValueChannel(int x, int y, int isp) const {
    assert((x >= 0)&&(x < width));
    assert((y >= 0)&&(y < height));
    return cols[(width * y + x)*3 + isp];
  }

  // Returns maximum value
  float GetMaxValue(CColor &col) const;
  
  // get the color when indexed as image
  bool GetRGB_XY(int x, int y, CColor &col) const;
  // access directly to the triple of floats
  float* GetRGB_XY(int x, int y) {return cols+3*(width*y+x);}
  // raw access to the data from beginning
  float *GetBuf() {return cols;}

  // get the color when indexed as image
  bool SetRGB_XY(int x, int y, float r, float g, float b);

  // make a mark at specific point in the image
  void MakeMarkRGB(int mx, int my, EMarkType typeMark,
		   int marksize = 2, float mult = 1.0f);
  
  // the whole image is filled in by the same color
  void SetConstant(float r, float g, float b);

  // shifts circularly the image to left or right
  void HorizontallyShift(int pixelOffset);

  // The whole image is applied power function of the original function
  void PowerFunction(float rexp, float gexp, float bexp);
  void Multiply(float coeff, int x1=0, int y1=0, int x2=-1, int y2=-1);
  int Mask(const CHDRImage &m, float threshold, int x1, int y1, int x2, int y2);
  int Mask(const CHDRImage &m, float threshold, float red, float green, float blue,
	   int x1, int y1, int x2, int y2);
  void AddOffset(float coeff, int x1=0, int y1=0, int x2=-1, int y2=-1);
  void Minimize(float coeff, int x1=0, int y1=0, int x2=-1, int y2=-1);
  void Maximize(float coeff, int x1=0, int y1=0, int x2=-1, int y2=-1);
  void Fabs(int x1=0, int y1=0, int x2=-1, int y2=-1);
  void MakeGray(int x1, int y1, int x2, int y2);
  void MakeWholeGray();
  float MinLuminanceValue() const;
  float MaxLuminanceValue() const;
  // Computes percentile between values (darkest=0.0 and brightest=1.0)
  float PercentileLuminanceValue(float ratio) const;
  // Subtract two images to the output file, possibly given by the user
  CHDRImage* Subtract(CHDRImage &i2, CHDRImage *i3 = 0,
		       bool computeRMSE = true, int verbose = true) const;
  // Set pseudocolor from image to the output i2 image
  int SetsPseudocolor(CHDRImage &i2, int pseudoColorTechnique,
		      float lowerBound, float upperBound) const;
};

// get the color when indexed as image
inline bool
CHDRImage::GetRGB_XY(int x, int y, CColor &col) const
{
  if ((x < 0)||(x >= width)) return true; // error
  if ((y < 0)||(y >= height)) return true; // error
  int itt = (width*y+x)*3;
  col[0] = cols[itt]; // red
  col[1] = cols[itt+1]; // green
  col[2] = cols[itt+2]; // blue
  return false;
}

// get the color when indexed as image
inline bool
CHDRImage::SetRGB_XY(int x, int y, float r, float g, float b)
{
  if ((x < 0)||(x >= width)) return true; // error
  if ((y < 0)||(y >= height)) return true; // error
  int itt = (width*y+x)*3;
  cols[itt] = r;
  cols[itt+1] = g;
  cols[itt+2] = b;
  return false;
}

// --------------------------------------------------------------------------------
// The declaration of environment map putting HDR image as sphere aroung the scene
class CEnvironmentMap:
  public CHDRImage
{
private:
  // only luminance when computed float32
  float *lums;
  // luminance in cummulative sum for inverse transform method
  double *cumlums;
  // luminance in cummulative sum for inverse transform method
  // second impl, as in PBRT book, first summing in columns, then in rows
  double *cumcols;
  // addressing CDF
  int AddrCum(int x, int y) const {return x*height+y;}
  // addressing PDF
  int AddrLum(int x, int y) const {return y*width+x;}
  // the total power integrated over the sphere in the image
  double totalPower, totalPowerOrig;
  float totalPowerInv;
  // positions and colours of precomputed lights
  float **envLightPositions;
  float **envLightColours;
  float **envLightPositionsBackup;
  float **envLightColoursBackup;
  float *envLightProbs;
  int cntAllocatedLights; // the number of allocated/precomputed lights
  int cntLights; // the number of lights
  // computation of variance
  int   cntDirsVDmax; // maximum number of directions
  int   cntDirsVDcur; // current number of directions
  // if to randomize selection from precomputed lights
  bool randomizePrecomputedLights;
  int randomizedIndexLight;
  // local functions
  float ComputeProb(float x, float y, float theta, CColor &rgb) const;  
  void DeallocatePrecomputedLights();
  void AllocatePrecomputedLights(int cnt);
  inline bool SetLUM_XY(int x, int y, float val);
  void InsertInitialSphereDirSamples(); // initial directions on the sphere
public:
  CEnvironmentMap();
  CEnvironmentMap(const CEnvironmentMap &src); // copy constructor
  ~CEnvironmentMap();

// If this macro is defined, then use classic coordinate system XYZ, where
// Z-axis is in the direction of north pole
// plane XY corresponds to theta=PI/2 and phi <0, 2PI)
// If macro is undefined, use classic coordinate system XYZ, but the north pole
// is in the direction of Y-axis 
#define XYZCOORSYSTEM
  
  // This is for vizualization
  bool SaveDebugHDR(const string &filename, int mode, float mult = 1.0);

  // Allocate the image for manual filling in
  virtual void Allocate(int nwidth, int nheight) {
    delete []lums; lums = 0;
    delete []cumlums; cumlums = 0;
    delete []cumcols; cumcols = 0;
    // Allocate only color values
    CHDRImage::Allocate(nwidth, nheight);
  }

  // Compute luminance from color values
  void ComputeLuminance();
  // Normalize the power of EM map so the integral is 1 Watt.
  void NormalizePower();
  // Compute cummulative distribution function (=CDF)
  bool ComputeCDF();
  // computes the total power
  void ComputeTotalPower();
  // returns total power
  double GetTotalPower() const { return totalPower;} 
  // returns total power original without possible normalization
  double GetTotalPowerOrig() const { return totalPowerOrig;}

  // get value RGB for a particular theta and phi in radians, when we assume
  // that the HDR image is the environment map
  bool GetRGB_ThetaPhi(float theta, float phi, CColor &rgb) const;
  // get value probability for a particular theta and phi in radians
  float GetProb_ThetaPhi(float theta, float phi) const;
  // This returns probability and sets RGB together, theta and phi in radians
  float GetRGBandProb_ThetaPhi(float theta, float phi, CColor &rgb) const;
  
  // get luminance when indexed as image, theta and phi in radians
  bool GetLUM_XY(int x, int y, float &lum) const;
  float GetLUM_DIRexact(float theta, float phi) const;
  float GetLUM_DIRapprox(float theta, float phi) const;
  // get cummulative sum of luminance when indexed as image by X and Y coordinates
  bool GetCUMLUM_XY(int x, int y, float &clum) const;  

  // For two random values rnd1 and rnd2 compute X-Y position on the
  // image and set the output RGB value
  bool ImportanceSamplingXY(float rnd1, float rnd2, 
			    float &X, float &Y, CColor &rgb) const;

  // For two random values rnd1 and rnd2 compute theta-phi position on the
  // full sphere and set the output RGB value
  bool ImportanceSamplingThetaPhi(float rnd1, float rnd2,  // input
				  float &theta, float &phi, // output in radians
				  CColor &rgb, float &prob) const; // output

  float ThetaToY(float theta) const {
    assert(0 <= theta); assert(theta <= M_PI);
    // bottom line (0) of the image is PI, (height-1)-line is then 0
    return (height-0.9999f)*theta/float(M_PI);
  }
  float YtoTheta(float y) const {
    assert(0 <= y); assert(y < height);
    return y/(height-0.9999f) * float(M_PI); // bottom line (0) of the image is PI
  }
  float PhiToX(float phi) const {
    assert(phi >= 0); assert(phi<=2.f*float(M_PI));
    return phi/(2.f*float(M_PI)) * (width-0.9999f);
  }
  float XtoPhi(float x) const {
    assert(x >= 0); assert(x < width);
    return (x*2.f*float(M_PI))/(width-0.999f);
  }
  // precompute cnt directional lights to approximate environment map
  void PrecomputeLights(int cnt);
  // how many lights are currently precomputed
  int  countPrecomputedLights() const { return cntLights;}
  
  // get current mode if randomization applies
  bool GetModeRandomLights() const { return randomizePrecomputedLights;}
  // set random access to precomputed lights
  void SetModeRandomLights(bool randomFlag) {
    randomizePrecomputedLights = randomFlag;
    if (randomFlag == true) {
      const int thresholdLights = 10000;
      if (cntLights < thresholdLights) {
	FATAL << "ERROR: For randomization of deterministic lights you have"
	      <<" to increase the number of lights to at least "
	      << thresholdLights << endl;
	FATAL_ABORT;
      }
    }
  }
  void GenerateNewRandomLightPrecomputed() {
    if (randomizePrecomputedLights) {
      randomizedIndexLight = int(RandomValue() * cntLights);
      assert(randomizedIndexLight>=0); assert(randomizedIndexLight<cntLights);
    }
  }
  // Accessing functions for direction for precomputed lights
  float GetDirectionPrecomputedLight(int i, int ixyz) const {
    if (randomizePrecomputedLights) { i = randomizedIndexLight;}
    assert(i>=0); assert(i<cntLights);
    assert(ixyz>=0); assert(ixyz<3);
    return envLightPositions[i][ixyz];
  }
  CVector3D GetDirectionPrecomputedLight(int i) {
    if (randomizePrecomputedLights) { i = randomizedIndexLight;}
    assert(i>=0); assert(i<cntLights);
    CVector3D dir(envLightPositions[i][0],
		  envLightPositions[i][1],
		  envLightPositions[i][2]);
    return dir;		  
  }
  void SetDirectionPrecomputedLight(int i, CVector3D &XYZ) {
    if (randomizePrecomputedLights) { i = randomizedIndexLight;}
    assert(i>=0); assert(i<cntLights);
    for (int isp=0; isp<3;isp++) envLightPositions[i][isp]=XYZ[isp];
  }

  // Accessing functions for colors for precomputed lights
  float GetColorPrecomputedLight(int i, int irgb) const {
    if (randomizePrecomputedLights) { i = randomizedIndexLight;}
    assert(i>=0); assert(i<cntLights);
    assert(irgb>=0); assert(irgb<3);
    return envLightColours[i][irgb];
  }
  float* GetColorPrecomputedLight(int i) {
    if (randomizePrecomputedLights) { i = randomizedIndexLight;}
    assert(i>=0); assert(i<cntLights);
    return envLightColours[i];
  }
  void SetColorPrecomputedLight(int i, CColor &RGB) {
    if (randomizePrecomputedLights) { i = randomizedIndexLight;}
    assert(i>=0); assert(i<cntLights);
    for (int isp=0; isp<3;isp++)
      envLightColours[i][isp] = RGB[isp];
  }

  // returns value of probability density function for a precompute light 'i'
  float GetProbPrecomputedLight(int i) const {
    if (randomizePrecomputedLights) { i = randomizedIndexLight;}
    assert(i>=0); assert(i<cntAllocatedLights);
    return envLightProbs[i];
  }
  void MakeLighter(int i, float c1, float c2);
  void RestoreColorFromBackup(int i) {
    assert(i >= 0); assert(i<cntAllocatedLights);
    for(int j=0; j < 3; j++)
      envLightColours[i][j] = envLightColoursBackup[i][j];
  }
  // rotates only precomputed lights around y-axis
  void RotateEnvironmentLights(float angle);
  // make mark in luminance channel in the image
  void MakeMarkLUM(int mx, int my, EMarkType typeMark, int marksize = 2,
		   float mult = 1.0f);

  // This is checking the importance sampling and other methods
  // for correctness, used for development only
  void Test_Random();
  // for debugging only
  // North Hemisphere
  void SetFunctionConstNorthHemisphere(float r, float g, float b);
  void SetFunctionCosineTheta(float r, float g, float b);
  void SetFunctionCosineThetaNorthHemisphere(float r, float g, float b);
  void SetFunctionCosineThetaPowNorthHemisphere(float r, float g, float b, float p);
  void SetFunctionCosineThetaPowSphere(float r, float g, float b, float p);
  // South hemisphere
  void SetFunctionConstSouthHemisphere(float r, float g, float b);

  // Some other testing functions setting the data
  void SetFunction2(float r, float g, float b);
  void SetFunction3(float r, float g, float b);
};

// ---------------------------------------------------------------------------
// Inline methods

// get luminance when indexed as image
inline bool
CEnvironmentMap::GetLUM_XY(int x, int y, float &lum) const
{
  if ((x < 0)||(x >= width)) return true; // error
  if ((y < 0)||(y >= height)) return true; // error
  lum = lums[width*y+x];
  return false;
}

// theta and phi in radians
inline float
CEnvironmentMap::GetLUM_DIRexact(float theta, float phi) const
{
  float yf = ThetaToY(theta);
  int iy = (int)yf;
  assert(yf>=0); assert(yf<height);
  int iy2 = iy+1;
  if (iy2 >=height) iy2 = height-1;
  float beta = yf - iy;

  float xf = PhiToX(phi);
  assert(xf>=0); assert(xf<width);
  int ix = (int)ix;
  int ix2 = (ix+1)%width;
  float alpha = xf - ix;
  float lumsExact = (lums[width*iy+ix]*(1.f-alpha)+lums[width*iy+ix2]*alpha)*(1.f-beta)+
    (lums[width*iy2+ix]*(1.f-alpha)+lums[width*iy2+ix2]*alpha)*(1.f-beta);
  return lumsExact;
}

// theta and phi in radians
inline float
CEnvironmentMap::GetLUM_DIRapprox(float theta, float phi) const
{
  int iy = (int)(ThetaToY(theta)+0.5f); // rounded to the first coordinate
  if (iy >= height) iy=height-1;
  int ix = ((int)(PhiToX(phi)+0.5))%width;
  assert(iy>=0); assert(iy<height);
  assert(ix>=0); assert(ix<width);
  return lums[width*iy+ix]; // nearest neighbor value
}

// get cummulative sum of luminance when indexed as image
inline bool
CEnvironmentMap::GetCUMLUM_XY(int x, int y, float &clum) const
{
  if ((x < 0)||(x >= width)) return true; // error
  if ((y < 0)||(y >= height)) return true; // error
  clum = float(cumlums[width*y+x]);
  return false;
}

__END_GOLEM_HEADER

#endif // __HDRIMG_H__


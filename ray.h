// ============================================================================
//
// ray.h
//	  CRay class - core of the ray-tracer design.
//
// Class: CRay
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 1997.

#ifndef __RAY_H__
#define __RAY_H__

// standard headers
#include <vector>

// nanoGOLEM headers
#include "matrix4.h"
#include "vector3.h"

__BEGIN_GOLEM_HEADER

// forward declarations
class CObject3D;

// -------------------------------------------------------------------
// CRay class.  A ray is defined by a location and a direction.
// The direction is always normalized (length == 1).
// -------------------------------------------------------------------
class CRay
{
public:
  // constructors
  CRay(const CVector3D &wherefrom, const CVector3D &whichdir) {
    loc = wherefrom;
    dir = Normalize(whichdir);
    Init();
  }
  // dummy constructor
  CRay() {}

  // Inititalize the ray again when already constructed
  void Init(const CVector3D &wherefrom, const CVector3D &whichdir,
	    bool dirNormalized = false) {
    loc = wherefrom;
    dir = (dirNormalized) ? whichdir: Normalize(whichdir) ;
    Init();
  }

  // --------------------------------------------------------
  // Extrapolate ray given a signed distance, returns a point
  // --------------------------------------------------------
  CVector3D Extrap(float t) const {
    return loc + dir * t;
  }

  // -----------------------------------
  // Reflects direction of reflection for the ray, 
  // given the normal to the surface.
  // -----------------------------------
  CVector3D ReflectRay(const CVector3D &N) const {
    return N * 2.0 * DotProd(N, -dir) + dir;
  }

  // Given the matrix 4x4, transforms the ray to another space
  void ApplyTransform(const CMatrix4 &tform) {
    loc = tform * loc;
    dir = RotateOnly(tform, dir);
    Precompute();
  }

  // returns ID of this ray (use for mailboxes)
  int GetID() const { return ID; }

  // set unique ID for a given ray - always avoid setting to zero
  void SetID() {
    if ((ID = ++genID) == 0)
      ID = ++genID;
  }
  // set ID to explicit value - it can be even 0 for rays transformed
  // to the canonical object space to supress the mailbox failure.
  void SetID(int newID) {
    ID = newID;
    // note that transfID is not changed!
  }

  inline void SetLoc(const CVector3D &l);
  const CVector3D& GetLoc() const { return loc; }
  CVector3D  GetLoc() { return loc; }

  void SetDir(const CVector3D &ndir) { dir = ndir;} 
  const CVector3D& GetDir() const { return dir; }
  CVector3D GetDir() { return dir; }

private:
  CVector3D loc, dir;		// Describes ray origin and vector

  // unique ID of a ray for the use in the mailboxes
  int ID;
  /// Precompute some CRay parameters. Most of them used for ropes traversal.
  inline void Init();

  // generator ID
  static int genID;

  // Precompute some values that are necessary.
  inline void Precompute();
};

// --------------------------------------------------------------------------
//  CRay::SetLoc()
// --------------------------------------------------------------------------
inline void
CRay::SetLoc(const CVector3D &l)
{
  loc = l;
}

// --------------------------------------------------------------------------
//  CRay::Precompute()
// --------------------------------------------------------------------------
inline void
CRay::Precompute()
{
  SetID();
}

// --------------------------------------------------------------------------
//  CRay::Init()
// --------------------------------------------------------------------------
inline void
CRay::Init()
{
  // apply the standard precomputation
  Precompute();
}

__END_GOLEM_HEADER

#endif // __RAY_H__


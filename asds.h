// ===================================================================
//
// asds.h
//     Auxiliary spatial data structures for computation
//     of intersection of a ray with objects in the scene
//
// Class: CASDS_Base, CASDS, ASDS_BB, CAObject, CAObjectList, CNaiveRSA
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file
// doc/Licence.txt
//
// Initial coding by Vlasta Havran, 1998

#ifndef __ASDS_H__
#define __ASDS_H__

// nanoGOLEM headers
#include "configh.h"
#include "sseutils.h"
#include "sbbox.h"
#include "triangle.h"


// standard headers
#include <list>

__BEGIN_GOLEM_HEADER

// forward declarations
class CRay;
class CStatistics;
class CVector3D;

// --------------------------------------------------------
// CASDS_Base
//
// Abstract class CASDS_Base, that contains only basic definitions
// and no data. It does not tell anything about the interior
// representation of search structure or whether the objects
// are stored somewhere or anything else.
// --------------------------------------------------------
class CASDS_Base {
public:
  // constructor
  CASDS_Base() {}

  // destructor removes the data .. explicitely virtual constructor
  virtual ~CASDS_Base() {}

  // enum of ID for different classes
  enum EASDS_ID {
    ID_ASDS_None,
    ID_ASDS_Base,
    ID_ASDS,
    ID_ASDS_BB,
    ID_AObjectList,
    ID_NaiveRSA,
    ID_BVHTopDown,
    ID_BVHSAH
  };

  // returns the identification of the class
  virtual EASDS_ID GetID() const = 0;

  // returns the bounding box where this data structure has its validity
  virtual SBBox BBox() const = 0;

  // FindNearest returns pointer to the first object hit by the ray.
  // If the ray hits the object; it passes back signed distance in info.t,
  // and returns the pointer to that object and sets it to info.object.
  // If no intersection exist, NULL is returned. This function is designed
  // to be used in application code!
  const CObject3D *FindNearest(CRay &ray, CHitPointInfo &info);
  unsigned FindNearest(CRayPacket &rayPacket, CRPHitPointsInfo &packetInfo);

  // and some basic parameters to given stream
  virtual void ProvideID(ostream &app) = 0;

  // removes all data structures connected with this structure
  virtual void Remove() = 0;

  // do any initialization that is required before traversing the ASDS,
  // when this ASDS is used via function FindNearest()
  virtual void TraversalReset();

protected:
  // Interior implementations of the functions above

  // FindNearestI returns pointer to the first object hit by the ray.
  // If the ray hits the object; it passes back signed distance in info.t,
  // and returns the pointer to that object and sets it to info.object.
  // If no intersection exist, NULL is returned.
  // IMPORTANT - info.maxt must not be changed at all
  virtual const CObject3D *FindNearestI(CRay &ray, CHitPointInfo &info) = 0;
  virtual unsigned FindNearestI(CRayPacket &rayPacket, CRPHitPointsInfo &packetInfo);
};

// ---------------------------------------------------------
// CASDS
//     Contains abstract class for support auxiliary
//     spatial data structures for computation of intersection
//     of ray with objects in the scene. It contains the
//     copy of the object list for which the objects were constructed.
//     It contains the function which returns the axis aligned bounding
//     box of space for which this CASDS operates.
//
// ---------------------------------------------------------
class CASDS : public CASDS_Base {
protected:
  // the copy of the original list of objects which were passed
  // to build up this data structure
  CObjectList *objlist;

  // the flag if the CASDS was build up
  bool builtUp;

  // parameter of the nearest intersection of this CASDS
  float tInt;

  // ID number of the last gathered statistics
  int statsID;

public:
  // typedef that can be further used
  typedef vector<CASDS *> TASDSList;

  // constructor
  CASDS();

  // destructor removes the data .. shall be explicitely virtual constructor
  virtual ~CASDS();

  virtual bool HandleUnboundeds() const { return false; }

  virtual EASDS_ID GetID() const { return ID_ASDS; }

  // builds up auxiliary data structures for ray-intersection calculation
  virtual void BuildUp(const CObjectList & /*objlist*/) {}

  // remove all dynamic data structures used by this class
  virtual void Remove();

  // returns if this CASDS was built up or not
  bool IsConstructed() const { return builtUp; }

  // Given the list of ASDSs, computes the enclosing bounding box for all ASDSs
  static void ComputeBoxForListOfASDS(SBBox &boxToInclude,
                                      const TASDSList &asdslist);

  // given the name of the structure, allocate the ASDS and return the
  // non-zero pointer if successfull
  static CASDS *Allocate(const CASDS_Base::EASDS_ID &dstruct);

  // This function should be used on the object list to start from the offset
  // for visibility faking and reading/writing ASDS from/to a file
  static void ReassignUniqueIDs(CObjectList &objList);
};

// ---------------------------------------------------------------------------
// the same abstract class but it contains the bounding box of the whole scene
class CASDS_BB : public CASDS {
protected:
  // the bounding box enclosing the extent of this spatial data structure
  SBBox bbox;

public:
  // constructor
  CASDS_BB() : bbox(SBBox(MAXFLOAT, -MAXFLOAT)) {}

  // destructor
  virtual ~CASDS_BB() {}

  // returns the bounding box of this data structure
  virtual SBBox BBox() const { return bbox; }

  virtual EASDS_ID GetID() const { return ID_ASDS_BB; }

  void InitializeBox(SBBox &bbox, CObjectList &objlist) {
    if (objlist.size() == 0) {
      FATAL << "Initializing the box with zero primitives" << endl;
      FATAL_ABORT;
    }
    for (CObjectList::iterator it = objlist.begin(); it != objlist.end();
         it++) {
      bbox.Include((*it)->GetBox());
    }
  }
};

// ---------------------------------------------------------------------------
// The class represents only a list of objects, where the traversal
// is performed. It can/should be used inside other ASDS.
class CAObjectList : public CASDS_BB {
public:
  // constructor
  CAObjectList() {}

  // destructor
  virtual ~CAObjectList();

  // builds up auxiliary data structures for ray-intersection calculation
  virtual void BuildUp(const CObjectList &objl);

  // builds up auxiliary data structures for ray-intersection calculation
  // and sets the bounding box to a specific instance
  void BuildUp(const CObjectList &objl, const SBBox &nBox);

  virtual EASDS_ID GetID() const { return ID_AObjectList; }

  virtual int ListCount() const { return static_cast<int>(objlist->size()); }

  CObjectList *GetObjList() { return objlist; }

  // removes all data structures connected with the CASDS
  virtual void Remove();

  // provides some identification of the spatial data structure
  // and some basic parameters to given stream
  virtual void ProvideID(ostream &app) {
    app << "CAObjectList #objects = " << objlist->size() << "\n";
  }

  // it should print some statistics on standard output .. important
  virtual void PrintStats() {} // especially for debugging

protected:
  // NearestInt returns 1 if the ray hits the object; it passes back
  // the parameter of intersection in signsed distance t.
  // If no intersection exist, NULL is returned.
  virtual const CObject3D *FindNearestI(CRay &ray, CHitPointInfo &info);
};

// ---------------------------------------------------------------------------
// The class represents standalone class with a list of objects,
// where traversal is performed. It must not be used within other ASDS !
class CNaiveRSA : public CAObjectList {
public:
  // constructor
  CNaiveRSA() : CAObjectList() {}

  virtual EASDS_ID GetID() const { return ID_NaiveRSA; }

  // builds up auxiliary data structures for ray-intersection calculation
  virtual void BuildUp(const CObjectList &objl);

  // builds up auxiliary data structures for ray-intersection calculation
  // and sets the bounding box to a specific instance
  void BuildUp(const CObjectList &objl, const SBBox &nBox);

  // provides some identification of the spatial data structure
  // and some basic parameters to given stream
  virtual void ProvideID(ostream &app) {
    app << "CNaiveRSA #objects = " << objlist->size() << "\n";
  }
};

__END_GOLEM_HEADER

#endif // __ASDS_H__

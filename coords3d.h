// ================================================================
//
// coords3d.h
//     Coordinates conversions
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 2000.

#ifndef __COORDS3D_H__
#define __COORDS3D_H__

// Standard headers
#include <cassert>
#include <cmath>

// nanoGOLEM headers
#include "configh.h"
#include "vector3.h"

__BEGIN_GOLEM_HEADER

// Some functions designed only for hemisphere

// input theta and phi in radians, output xyz is the unit vector
void
ConvertThetaPhiToXYZ(float theta, float phi, CVector3D &xyz);

// The method that converts the directional vector on the hemisphere to the 
// angle theta (from zenith) and phi angle (the azimuth)
void
ConvertXYZtoThetaPhi(const CVector3D& xyz, // input
                     float &theta, // output
                     float &phi);

__END_GOLEM_HEADER

#endif // __COORDS3D_H__


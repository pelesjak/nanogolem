// ===================================================================
//
// world.h
//     Header file for CWorld class.
//
// Class: CWorld
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 2000.

#ifndef __WORLD_H__
#define __WORLD_H__

// standard headers
#include <string>

// nanoGOLEM headers
#include "configh.h"
#include "camera.h"
#include "environ.h"

__BEGIN_GOLEM_HEADER

// forward declarations
class CScene;
class CSynthesisApp;

// ---------------------------------------------------------
// CWorld 
//   Contains all the information required to perform 
//   some operations on a set of objects.
// ---------------------------------------------------------
class CWorld
{
public:
  // Pointers to the world components, when they are used:
  // The instances used in world:
  //
  // Environment settings for this world
  CEnvironment   _environment;

  // An application algorithm that works over scene + environment + statistics
  CSynthesisApp *_application;
  // The scene including objects, lights, surfaces etc.
  CScene        *_scene;
  // The camera to create an image
  CCamera        _camera;
  // The result of the computation is usually saved to a file
  // with the following name
  string         outputFileName;

  void SetDefaultValues();
public:
  // -------------------------------------------------------
  // Constructor sets some default values.
  // -------------------------------------------------------
  CWorld(const int argc, const char *argv[]);
  virtual ~CWorld();

  // -------------------------------------------------------
  // Functions to set parameters of the World.
  // -------------------------------------------------------
  void SetScene(CScene *scene) {
    _scene = scene;
  }

  // parse the scene, returns false if everything OK
  bool ParseScene(const string &sceneFilename);

  // initializes the variables
  bool Init(const string &inputFileName);

  // creates application depending on the application type set
  bool CreateApplication(void);

  // deletes application so next one can be created again
  void DeleteApplication(void);
  
  // returns application of this world
  const CSynthesisApp* GetApplication(void) const {
    return _application;
  }

  // This makes the world into the run for the world
  bool Run();

  // Delete and clean all structures for further use of world
  // without its destruction
  virtual void CleanUp();
};

// ----------------------------------------------------------------
// the only global variables in nanoGOLEM rendering system,
// that points to the currently processed world. These point
// to the identical pointer. These two variables have to
// be declared in the file where main() function is defined.

// global world pointer
extern CWorld *world;

__END_GOLEM_HEADER

#endif // __WORLD_H__

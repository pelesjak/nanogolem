// ===================================================================
//
// talloc.cpp
//     Allocation/deallocation routines for old-style Numerical recipes
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Jiri Filip, 2001

// Standard headers
#include <cstdio>
#include <cassert>
#include <cstdlib>

// nanoGOLEM headers
#include "configg.h"
#include "errorg.h"
#include "talloc.h"

__BEGIN_GOLEM_SOURCE

float*
allocation1(int nrl,int nrh)
{
  float *m = 0;
  if (nrh-nrl+1 <= 0)
    return 0;

  m = (float*)calloc((unsigned)(nrh-nrl+1), sizeof(float));
  assert(m);
  m -=nrl;

  return m;
} // allocation1 --------------------------------------------- 

float**
allocation2(int nrl, int nrh, int ncl, int nch)
{
  int j;
  float **m = 0;
  if ((nrh-nrl+1 <= 0) || (nch-ncl+1 <= 0))
    return 0;
  

  m = (float**)calloc((unsigned)(nrh-nrl+1), sizeof(float*));
  assert(m);
  if (!m) {
    FATAL << "Allocation failure 1 in allocation2()" << endl;
  }
  m -=nrl;

  for(j=nrl; j <= nrh; j++){
    m[j] = (float*)calloc((unsigned)(nch-ncl+1),sizeof(float));
    assert(m[j]);
    if (!m[j]) {
      FATAL << "Allocation failure 2 in allocation2()" << endl;
    }
    m[j] -= ncl;
  }

  return m;
} // allocation2 ----------------------------------------------- 

float***
allocation3(int nmf, int nml, int nrl, int nrh, int ncl, int nch)
{
  int i,j;
  float ***m = 0;
  if ((nml-nmf+1 <= 0) || (nrh-nrl+1 <= 0) || (nch-ncl+1 <= 0))
    return 0;

  // allocate pointers to matrixs 
  m = (float***)calloc((unsigned)(nml-nmf+1), sizeof(float**));
  if (!m) {
    FATAL << "Allocation failure 1 in allocation3()" << endl;
  }
  m -= nmf;

  // allocate pointers to rows 
  for(j = nmf; j <= nml; j++){
    m[j] = (float**)calloc((unsigned)(nrh-nrl+1), sizeof(float*));
    if (!m[j]) {
      FATAL << "Allocation failure 2 in allocation3()" << endl;
    }
    m[j] -= nrl;

    // allocate rows and set pointers to them 
    for (i = nrl; i <= nrh; i++){
      m[j][i] = (float*) calloc((unsigned)(nch-ncl+1), sizeof(float));
      if (!m[j][i]) {
	FATAL << "Allocation failure 3 in allocation3()" << endl;
      }
      m[j][i] -= ncl;
    }
  }
  return m;
} //-- allocation3 --------------------------------------------- 

//###################################################################
// DEALLOCATIONS                                                                
//###################################################################

void
freemem1(float *m,int nrl,int nrh)
{
  // This method frees memory allocated by allocation1 
  nrh = nrh;
  free((float*)(m+nrl));
} // freemem1 ---------------------------------------------------- 

void
freemem2(float **m, int nrl, int nrh, int ncl, int /*nch*/)
{
  //This function frees memory allocated by allocation2 
  int i;
  //  nch=nch;
  //  nrh=nrh;

  for (i= nrh; i >= nrl; i--) free((float*)(m[i]+ncl));
  free((float**)(m+nrl));
} // freemem2 --------------------------------------------------- 

void
freemem3(float ***m, int nmf, int nml, int nrl, int nrh, int ncl, int nch)
{
  int i,j;
  nch = nch;

  for (j = nml; j >= nmf; j--){
    for (i = nrh; i >= nrl; i--)
      free((float*)(m[j][i]+ncl));
    free((float**)(m[j]+nrl));
  }
  free((float***)(m+nmf));
} // freemem3 --------------------------------------------- 

__END_GOLEM_SOURCE


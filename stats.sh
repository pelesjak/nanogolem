#!/bin/bash
make
FILE=stats_1.txt
for VIEW in A10 Armadillo_B city_B city2_B conference_B fforest_B park_B sibenik_B teapots_B
do
    for APP in RT00 RTSSE
    do
        for ASDS in SAHBVH TDBVH
        do
            for i in 01 02 03 04 05
            do
                echo -n "${i} - " >> ${FILE}
                ./bin/nanogolem -Eglobal.env -a${APP} -t${ASDS} -SON -F${FILE} scenes/${VIEW}.view
            done
        done
    done
done
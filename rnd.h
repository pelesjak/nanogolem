// ===================================================================
//
// rnd.h:
//     A set of functions to generate random numbers according to
//     various distributions.
//
// Class: RND, CPlaneHalton2p, CPlaneHaltonpp
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 2000

#ifndef __RND_H__
#define __RND_H__

// nanoGOLEM headers
#include "configh.h"
#include "basrnd.h"
#include "errorg.h"
#include "vector2.h"
#include "vector3.h"

__BEGIN_GOLEM_HEADER

const float RAND_MAX_FLT = (float)RAND_MAX;

const float INV_RAND_MAX_FLT = (float) (1.0 / ((double)RAND_MAX+1.0));

const double INV_RAND_MAX_DOUBLE = 1.0 / ((double)RAND_MAX+1.0);

const float TWO_TIMES_INV_RAND_MAX_FLT = float(2.0 / ((double)RAND_MAX_FLT+1.0));

class RND
{
public:
  /**
     Returns a pseudo-random integer between 0 and RAND_MAX, int.
     Range is [0; RAND_MAX]
  */
  static inline int randi(void)
  { return rand(); }

  /**
     Returns a pseudo-random float between 0 and 1. Range is [0;1), float.
  */
  static inline float rand01f(void)
  {
    //return rand() * INV_RAND_MAX_FLT ;
    return RandomValue();
  }

  // Uniform distribution
  static double normalDistribution(double x) {
    // calculate \sqrt{2\pi} upfront once
    static const double RT2PI = sqrt(4.0*acos(0.0));
    // calculate 10/\sqrt{2} upfront once
    static const double SPLIT = 10./sqrt(2.0);
    static const double a[] = {220.206867912376,221.213596169931,112.079291497871,33.912866078383,
			       6.37396220353165,0.700383064443688,3.52624965998911e-02};
    static const double b[] = {440.413735824752,793.826512519948,637.333633378831,296.564248779674,
			       86.7807322029461,16.064177579207,1.75566716318264,8.83883476483184e-02};  
    const double z = fabs(x);
    // Now N(x) = 1 - N(-x) = 1-\sqrt{2\pi}N'(x)\frac{P(x)}{Q(x)}
    //  so N(-x) = \sqrt{2\pi}N'(x)\frac{P(x)}{Q(x)}
    // now let \sqrt{2\pi}N'(z)\frac{P(x)}{Q(z)} = Nz
    // Therefore we have
    //     Nxm = N(x) = \sqrt{2\pi}N'(z)\frac{P(x)}{Q(z)} = Nz if x<0
    //     Nxp = N(x) = 1 - \sqrt{2\pi}N'(z)\frac{P(x)}{Q(z)} = 1-Nz if x>=0
    double Nz = 0.0;
  
    // if z outside these limits then value effectively 0 or 1 for machine precision
    if (z<=37.0) {
      // NDash = N'(z) * sqrt{2\pi}
      const double NDash = exp(-z*z/2.0)/RT2PI;
      if (z<SPLIT) {
        // here Pz = P(z) is a polynomial
        const double Pz = (((((a[6]*z + a[5])*z + a[4])*z + a[3])*z + a[2])*z + a[1])*z + a[0];
        // and Qz = Q(z) is a polynomial
        const double Qz = ((((((b[7]*z + b[6])*z + b[5])*z + b[4])*z + b[3])*z + b[2])*z + b[1])*z + b[0];
        // use polynomials to calculate N(z)  = \sqrt{2\pi}N'(x)\frac{P(x)}{Q(x)}
        Nz = RT2PI*NDash*Pz/Qz;
      }
      else {
        // implement recurrence relation on F_4(z) 
        const double F4z = z + 1.0/(z + 2.0/(z + 3.0/(z + 4.0/(z + 13.0/20.0))));
        // use polynomials to calculate N(z), note here that Nz = N' / F
        Nz = NDash/F4z;
      }
    }
    // 
    return x>=0.0 ? 1-Nz : Nz;
  }

  /**
     Returns a pseudo-random float between 0 and 1. Range is (0;1], float.
  */
  static inline float rand01fNZ(void)
  {
    return (float)(rand()+1) * INV_RAND_MAX_FLT ;
  }

  /**
     Returns a pseudo-random float between 0 and 1. Range is [0;1), double.
  */
  static inline double rand01d(void)
  {
    // return (double) rand()  * INV_RAND_MAX_DOUBLE ;
    return (double) RandomValue();
  }
  
  /**
     Returns a pseudo-random float between -1 and 1. Range is [-1;1), float.
  */
  static inline float rand11f(void)
  {
    // return -1.0 + (float)rand() * TWO_TIMES_INV_RAND_MAX_FLT ;
    return -1.0f + (float)RandomValue() * 2.0f;
  }
  
  /**
     Returns two pseudo-random floats between 0 and 1.
     Range is [0;1)x[0;1), float.
  */
  static inline CVector2D rand01v(void)
  {
    return CVector2D(rand01f(), rand01f());
  }   

  /**
     Returns two pseudo-random floats between -1 and 1.
     Range is [-1;1)x[-1;1), float.
  */
  static inline CVector2D rand11v(void)
  {
    return CVector2D(rand11f(), rand11f());
  }   

  /**
     Returns a pseudo-random float between 0 and b.
     Range is [0;b), float
  */
  static inline float rand0bf(float b)
  {
    return RandomValue() * b ;
  }

  /**
     Returns a pseudo-random float between 0 and b.
     Range is (0;b], float
  */
  static inline float rand0bfNZ(float b)
  {
    return ((float)(rand()+1) * INV_RAND_MAX_FLT) * b ;
  }

  /**
     Returns a vector of two pseudo-random floats between 0 and b.
     Range is [0;b) x [0;b), float.
  */
  static inline CVector2D rand0bv(float b)
  {
    return CVector2D(rand0bf(b), rand0bf(b));
  }
     
  /**
     Returns a vector of two pseudo-random floats between 0 and b.
     Range is (0;b] x (0;b], float.
  */
  static inline CVector2D rand0bvNZ(float b)
  {
    return CVector2D(rand0bfNZ(b), rand0bfNZ(b));
  }

  /**
     Returns a pseudo-random float between 'a' and 'b'.
     Range is [a;b) x [a;b), float.
  */
  static inline float randabf(float a, float b)
  {
    float range = fabs(a - b);
    //return ((float)rand() * INV_RAND_MAX_FLT) * range + ((a < b) ? a : b);
    return RandomValue() * range + ((a < b) ? a : b);
  }

  /**
     Generate random point on unit circle with density p(x) = 1/PI.

     @return   the generated point as CVector2D
  */
  static inline CVector2D disc_const(void)
  {
    CVector2D v;
    do {v = rand11v();}
    while(v.xx * v.xx + v.yy * v.yy > 1.0);
    return v;
  }

  /**
     The following macro can replace the previous inline method if 
     we want to use some other source of random numbers than built
     in pseudo-random generator.

     @param v          name of the resulting CVector2D
     @param generator  a call to a function which returns pseudo random
                       number between 0 and 1.
  */
#define RND_disc_const(v, generator) \
      do {v = generator ; v.xx*=2.0f; v.xx-=1.0; v.yy*=2.0f; v.yy-=1.0;} \
      while(v.xx*v.xx+v.yy*v.yy>=1.0)
  
  /**
     Generate random point in triangle with density p(x) = 1/A.

     @param v  two random numbers between 0 and 1.
     @return   the generated point as CVector3D in BARYCENTRIC coordinates
  */
  static inline CVector3D
  triangle_const(const CVector2D& v=rand01v())
  {
    float sqrtr1 = sqrt(v.xx);
    
    CVector3D w(1.0f - sqrtr1, (1.0f - v.yy) * sqrtr1, v.yy * sqrtr1);
#ifdef _DEBUG
    float sum = w.x + w.y + w.z;
    if ( (sum < 0.99) || ( sum > 1.01)) {
      FATAL << "RND Generation in barycentric coordinates failed\n";
      FATAL_EXIT;
    }
#endif // _DEBUG
    return w;
  }

  /**
     Generate random point in unit disk in 2D using concentric map
     with density p(x) = 1/A.

     @param v  two random numbers between 0 and 1.
     @return   the generated point as CVector2D in 2D spherical coordinates
               (R,FI), radius R in <0,1>, angle FI in <0,2*PI>
  */
  static inline CVector2D
  disk_const(const CVector2D & w = rand01v())
  {
    assert(w.xx>=0.0);
    assert(w.xx<=1.0);
    assert(w.yy>=0.0);
    assert(w.yy<=1.0);

    float vx = -1.0f + 2.0f*w.xx;
    float vy = -1.0f + 2.0f*w.yy;
    float r, phi;

    if (vx > -vy) {
      if (vx > vy) {
	r = vx; phi = float(M_PI_4) * (vy / vx);
	// This modification is required to get range <0;2*M_PI>
	if (phi<0.0)
	  phi += 2.0f * float(M_PI);
      }
      else {
	r = vy; phi = float(M_PI_4) * (2.0f - (vx / vy) );
      }
    }
    else {
      // It holds: vx <= -vy
      if (vx < vy) {
	r = -vx; phi = float(M_PI_4) * (4.0f + (vy/vx) );
      }
      else {
	r = -vy;
	if (vy != 0.0)
	  phi = float(M_PI_4) * (6.0f - (vx/vy) );
	else
	  phi = 0.0;
      }
    }
    
    assert(r<=1.00001);
    assert(phi>=-0.000001);
    assert(phi<= 2.0*M_PI+0.000001);    
    return CVector2D(r, phi);
  }
  
  /**
     Generate random point on unit sphere.

     @param  v(in)        two random numbers between 0 and 1
     @return the generated point as CVector3D
  */
  static inline CVector3D sph_const(const CVector2D &v = rand01v())
  {
    float tmp = 2.0f * float(sqrt(v.xx * (1.0 - v.xx)));
    float tmp2 = 2.0f * float(M_PI) * v.yy;
    return CVector3D(cos(tmp2) * tmp, sin(tmp2) * tmp, 1.0f - 2.0f * v.xx);
  }

  /**
     Generate random direction on unit hemisphere with density
     p(theta) = 1/(2*PI).

     @param  result(out)  resulting direction
     @param  v(in)        two random numbers between 0 and 1

     @return the probability density at generated direction
             (which is always 1/(2*PI) for this method)
  */
  static inline float
  hemisph_const(CVector3D &result, const CVector2D &v = rand01v())
  {
    float sin_theta = sqrt(1.0f - v.xx * v.xx);
    assert(sin_theta <= 1.0f);
    assert(sin_theta >= 0.0f);
    float phi       = 2.0f * float(M_PI) * v.yy;
    result.Set( cos(phi)*sin_theta, sin(phi)*sin_theta, v.xx );
    return 1.0f/(2.0f*float(M_PI));
  }

  // The inverse function for the one above - getting random values
  static inline void
  hemisph_inv_const(CVector2D &result, float &dist, const CVector3D &v)
  {
    dist = Magnitude(v); // the magnitude of the vector
    CVector3D vv = v;
    vv *= 1.0f/dist; // normalize
    result.xx = vv.z;
    result.yy = atan2(vv.y, vv.x);
    if (result.yy < 0.f) result.yy += float(2.0*M_PI);
    result.yy *=1.0f/float(2.0*M_PI);
  }
  
  /**
     Generate random direction on unit hemisphere with uniform density.

     @param result(out) resulting direction
     @param v(in)       two random numbers discribing the radius and angle
                        in two dimensions, assuming uniform density on a disk
			v.xx .. radius in <0,1>, v.yy .. angle in <0,2*PI>

     @return the probability density at generated direction
  */
  static inline float
  hemisph_from_disk(CVector3D &result, const CVector2D &v)
  {
    float tmp2 = 1.0f - v.xx * v.xx;
    float tmp3 = v.xx * sqrt(1.0f + tmp2);
    result.Set( cos(v.yy) * tmp3, sin(v.yy) * tmp3, tmp2);
    return 1.0f/(2.0f*float(M_PI));
  }
  
  /**
     Generate random direction on hemisphere with theta<theta_max with density
     p(theta) = 1/(2*PI*(1-cos(theta_max)) .

     @param  result(out)  resulting direction
     @param  cos_t_max    cosinus of maximaml theta
     @param  v(in)        two random numbers between 0 and 1

     @return the probability density at generated direction
             (which is always 1/(2*PI*(1-cos(theta_max)) for this method)
  */
  static inline float
  hemisph_const_clamp(CVector3D &result, float cos_t_max,
		      const CVector2D &v = rand01v())
  {
    float tmp = sqrt(1 - sqr (1 - v.xx * (1.0f - cos_t_max) )  );
    float tmp2 =  2.0f * float(M_PI) * v.yy;
    result.Set( cos(tmp2) * tmp, sin(tmp2) * tmp,
		     1.0f - v.xx * (1.0f - cos_t_max) );
    return 1.0f/(2.0f * float(M_PI) * (1.0f - cos_t_max));
  }

  /**
     Generate random direction on unit hemisphere with density
     p(theta) = cos(theta) / PI .

     @param result(out) resulting direction
     @param v(in)       two random numbers between 0 and 1

     @return the probability density at generated direction
  */
  static inline float
  hemisph_cos(CVector3D &result, const CVector2D &v = rand01v())
  {
    float tmp     = sqrt(1.0f - v.xx);
    float tmp2    = 2.0f*float(M_PI)*v.yy;
    float cos_out = sqrt(v.xx);
    result.Set( cos(tmp2) * tmp, sin(tmp2) * tmp, cos_out );
    return cos_out * float(M_1_PI);
  }

  /**
     Generate random direction on unit hemisphere with density
     p(theta) = (n+1)/(2*PI) * cos^n(theta) .

     @param result(out) resulting direction
     @param n(in)       exponent in density
     @param v(in)       two random numbers between 0 and 1

     @return the probability density at generated direction
  */
  static inline float
  hemisph_cosn(CVector3D &result, float n, const CVector2D &v = rand01v())
  {
    float tmp2 = powf((float)(v.xx), (float)(1.0/(n + 1.0)));
    float tmp3 = sqrt(1.0f - tmp2 * tmp2);
    float tmp4 =  2.0f*float(M_PI)*v.yy;
    result.Set( cos(tmp4) * tmp3, sin(tmp4) * tmp3, tmp2);
    return powf(tmp2, n) * (n + 1.0f) * (0.5f*float(M_1_PI));
  }

  // A normal(=non-uniform=Gaussian) distribution with standard deviation
  // sigma = 1.0 and mean value <0.0, 0.0f>.
  // This implements the method of Bo, Muller, Marsaglia, 1958 (Annals
  // Math. Stat. 29, page 610-611. (Knuth, volume 2, page 122.
  // It generates two independent variables. It returns squared distance
  // from the origin (point 0,0)
  static inline float
  normal2D(CVector2D &result)
  {
    CVector2D v;
    float S;
  NEXT:
    do {
      v = rand11v();
      S = sqr(v.xx) + sqr(v.yy);
    } while (S >= 1.0f);
    assert(S <= 1.0f);
    if (S < 1e-6)
      goto NEXT; // we cannot handle infinity
    // compute the result by renormalization of squared distance
    float norm = sqrt(-2.0f/S * log(S) );
    result.xx = v.xx * norm;
    result.yy = v.yy * norm;
    return norm;
  }
  
};

// -----------------------------------------------------------
// Quasi random generators
// -----------------------------------------------------------

/**
   Class for incremental generating Halton low-disrepancy sequences in a
   plane. The firs coordinate is generated using base 2, the second one
   using base given in constructor.
*/
class CPlaneHalton2p
{
  int _p2; 
  int _k; // seed
  float _ip; // reciprocals of p2
public:
  // The base for second coordinate and the starting seed.
  CPlaneHalton2p(int p2, int k=0):
    _p2(p2), _k(k), _ip(1.0f/float(p2)) { }

  void SetSeed(int k) { _k = k; }

  // Generates two random variables in the range <0.0 ; 1.0> using
  // Halton sequences of base 2 and specified _p2;
  CVector2D next(void)
  {
    //    DEBUG << "CPlaneHalton2p::next()" << endl;
    //assert("a"==0);
    float p, u, v;
    int   kk, a;
  
    // first coordinate
    u = 0;
    for (p = 0.5, kk = _k ; kk ;  p *= 0.5, kk >>= 1)
      if (kk & 1)                          // kk mod 2 == 1
	u += p;
	
    // second coordinate
    v = 0;
    for (p = _ip, kk = _k ; kk ; p *= _ip, kk /= _p2)
      if ((a = kk % _p2))
	v += a * p;

    _k++;
    return CVector2D(u,v);
  }
};


// In this case we specify base for x-coordinate, base for y-coordinate
// and finaly the starting seed.
class CPlaneHaltonpp
{
  int _p1, _p2;
  int _k; // seed
  float _ip1, _ip2; // reciprocals of _p1 and _p2
public:
  CPlaneHaltonpp(int p1, int p2, int k=0):
    _p1(p1), _p2(p2), _k(k),
    _ip1(1.0f/float(p1)), _ip2(1.0f/float(p2)) { }

  void SetSeed(int k) { _k = k; }
  
  // Generates two random variables in the range <0.0 ; 1.0> using
  // Halton sequences of base _p1 and _p2
  CVector2D next(void)
  {
    float p, u=0, v=0;
    int kk, a;
  
    // the first coordinate
    for (p = _ip1, kk = _k ; kk ;  p *= _ip1, kk /= _p1)   
      if ((a = kk % _p1))
	u += a * p;
    
    // the second coordinate
    for (p = _ip2, kk = _k ; kk ; p *= _ip2, kk /= _p2)
      if ((a = kk % _p2))
	v += a * p;

    _k++;
    return CVector2D(u,v);
  }
};

__END_GOLEM_HEADER

#endif // __RND_H__


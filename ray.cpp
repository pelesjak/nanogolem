// ===================================================================
//
// ray.cpp
//
//      CRay class .. defining ray (oriented half line in R^3)
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran
// ===================================================================

// nanoGOLEM headers
#include "configg.h"
#include "ray.h"

__BEGIN_GOLEM_SOURCE

// =========================================================
// CRay .. static item used for generation of unique ID for
// each instantiated ray
int
CRay::genID = 1;

__END_GOLEM_SOURCE


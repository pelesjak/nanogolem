

#ifndef __BVHSAH_H__
#define __BVHSAH_H__

// standard C++ headers
#include <iostream>
#include <ostream>

using namespace std;

// nanoGOLEM headers
#include "configh.h"
#include "tdbvh.h"
#include "sbbox.h"

__BEGIN_GOLEM_HEADER

// forward declarations
class CObjectList;
class CObject3D;
class CRay;

// ----------------------------------------------------------
class CBVHSAH: public CTDBVH 
{
protected:
  // -------------------------------------------------------------------
  // Helper structure for creating BVH with SAH
  struct SBBoxedObject3D {
    double sizeLeft;
    double sizeRight;
    SBBox bbox;
    CObject3D *obj;
    CVector3D bboxCentroid;
  };
  SBBoxedObject3D *boxedObjects;

  class CompareBBoxedObject3D { 
    public:  
      bool operator() (SBBoxedObject3D &a, SBBoxedObject3D &b) { 
        return a.bboxCentroid[dim] < b.bboxCentroid[dim]; 
      }
      int dim; 
  };

  CompareBBoxedObject3D cmp;

  double rootSize;
  
  // The function for building recursively - using spatial median and SAH
  SBVHnode *BuildRecursivelyWithSAH(int firstIndex, int lastIndex);

public:
  // default constructor
  CBVHSAH();
  virtual void BuildUp(const CObjectList &objlist);
  virtual EASDS_ID GetID() const { return ID_BVHSAH;}  
  
  virtual void ProvideID(ostream &app);  
};

__END_GOLEM_HEADER

#endif // __BVHSAH_H__

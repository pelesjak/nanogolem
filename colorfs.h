// ===================================================================
//
// colorfs.h
//          Header file for color mapping of scalar values
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 2007

#ifndef __COLORFS_H__
#define __COLORFS_H__

// nanoGOLEM headers
#include "configh.h"
#include "color.h"

// From black to white, monochromatic
inline void
GetPseudoColor001(float value, float minVal, float maxVal,
		  CColor &RGB)
{
  if (isnan_local(RGB[0])||isnan_local(RGB[1])||isnan_local(RGB[2])) {
    RGB[0]=RGB[1]=RGB[2]=0;
    return;
  }  
  if (value < minVal) value = minVal;
  if (value > maxVal) value = maxVal;

  float ratio = (value - minVal)/(maxVal - minVal);
  // The first color - corresponding to minimum
  const float minColorRed = 0.f;
  const float minColorGreen = 0.f;
  const float minColorBlue = 0.f;
  // The second color - corresponding to maximum
  const float maxColorRed = 1.0f;
  const float maxColorGreen = 1.0f;
  const float maxColorBlue = 1.0f;

  // red
  RGB[0]= maxColorRed * ratio + (1.0f-ratio)*minColorRed;
  // green
  RGB[1]= maxColorGreen * ratio + (1.0f-ratio)*minColorGreen;
  // blue
  RGB[2]= maxColorBlue * ratio + (1.0f-ratio)*minColorBlue;

  return;
}

// From blue to red
inline void
GetPseudoColor002(float value, float minVal, float maxVal,
		  CColor &RGB)
{
  if (isnan_local(RGB[0])||isnan_local(RGB[1])||isnan_local(RGB[2])) {
    RGB[0]=RGB[1]=RGB[2]=0;
    return;
  }  
  if (value < minVal) value = minVal;
  if (value > maxVal) value = maxVal;

  float ratio = (value - minVal)/(maxVal - minVal);
  // The first color - corresponding to minimum
  const float minColorRed = 0.f;
  const float minColorGreen = 0.f;
  const float minColorBlue = 0.5f;
  // The second color - corresponding to maximum
  const float maxColorRed = 1.0f;
  const float maxColorGreen = 0.0f;
  const float maxColorBlue = 0.f;

  // red
  RGB[0]= maxColorRed * ratio + (1.0f-ratio)*minColorRed;
  // green
  RGB[1]= maxColorGreen * ratio + (1.0f-ratio)*minColorGreen;
  // blue
  RGB[2]= maxColorBlue * ratio + (1.0f-ratio)*minColorBlue;

  return;
}

// From green to red
inline void
GetPseudoColor003(float value, float minVal, float maxVal,
		  CColor &RGB)
{
  if (isnan_local(RGB[0])||isnan_local(RGB[1])||isnan_local(RGB[2])) {
    RGB[0]=RGB[1]=RGB[2]=0;
    return;
  }  
  if (value < minVal) value = minVal;
  if (value > maxVal) value = maxVal;

  float ratio = (value - minVal)/(maxVal - minVal);
  // The first color - corresponding to minimum
  const float minColorRed = 0.f;
  const float minColorGreen = 1.f;
  const float minColorBlue = 0.f;
  // The second color - corresponding to maximum
  const float maxColorRed = 1.0f;
  const float maxColorGreen = 0.0f;
  const float maxColorBlue = 0.f;

  // red
  RGB[0]= maxColorRed * ratio + (1.0f-ratio)*minColorRed;
  // green
  RGB[1]= maxColorGreen * ratio + (1.0f-ratio)*minColorGreen;
  // blue
  RGB[2]= maxColorBlue * ratio + (1.0f-ratio)*minColorBlue;

  return;
}

// Particular mapping from blue through green to red
inline void
GetPseudoColor004(float value, float minVal, float maxVal,
		  CColor &RGB)
{
  if (isnan_local(RGB[0])||isnan_local(RGB[1])||isnan_local(RGB[2])) {
    RGB[0]=RGB[1]=RGB[2]=0;
    return;
  }  
  if (value < minVal) value = minVal;
  if (value > maxVal) value = maxVal;

  float ratio = (value - minVal)/(maxVal - minVal);

  const float MAX_COLOR_VALUE = 0.98f;

  switch ((int)((ratio)*3.9999f)) {
  case 0:
    assert(ratio <= 0.25f);
    RGB[0]= 0.0f; // red
    RGB[1]= ratio * 4.0f * MAX_COLOR_VALUE; // green
    RGB[2]= MAX_COLOR_VALUE; // blue
    break;
  case 1:
    assert( (ratio >= 0.25f) && (ratio <= 0.5f) );
    RGB[0]= 0.0f; // red
    RGB[1]= MAX_COLOR_VALUE; // green
    RGB[2]= MAX_COLOR_VALUE * (1.0f - 4.0f*(ratio-0.25f)) ; // blue
    break;
  case 2:
    assert( (ratio >= 0.5f) && (ratio <= 0.75f) );
    RGB[0]= (ratio-0.5f) * 4.0f * MAX_COLOR_VALUE; // red
    RGB[1]= MAX_COLOR_VALUE; // green
    RGB[2]= 0 ; // blue
    break;
  case 3:
    assert( (ratio >= 0.75f) && (ratio <= 1.f) );
    RGB[0]= MAX_COLOR_VALUE; // red
    RGB[1]= MAX_COLOR_VALUE * (1.0f - 4.0f*(ratio-0.75f)); // green
    RGB[2]= 0 ; // blue
    break;
  default:
    WARNING << "BUG in GetPseudoColor003 !" << endl;
    break;
  }
  
  return;
}

// Rainbow color mapping by Jiri Bittner
inline void
GetPseudoColor005(float valueV, float minVal, float maxVal,
		  CColor &RGB)
{
  if (isnan_local(RGB[0])||isnan_local(RGB[1])||isnan_local(RGB[2])) {
    RGB[0]=RGB[1]=RGB[2]=0;
    return;
  }  
  if (valueV < minVal) valueV = minVal;
  if (valueV > maxVal) valueV = maxVal;

  float ratio = (valueV - minVal)/(maxVal - minVal);  
  float value = 1.0f - ratio;
  const float MAX_COLOR_VALUE = 0.999f;

  switch ((int)(value*4.0f)) {
  case 0:
    RGB[0]= MAX_COLOR_VALUE; // red
    RGB[1]= value*MAX_COLOR_VALUE; // green
    RGB[2]= 0.f; // blue
    break;
  case 1:
    RGB[0]= (1.0f - value)*MAX_COLOR_VALUE; // red
    RGB[1]= MAX_COLOR_VALUE; // green
    RGB[2]= 0.f; // blue
    break;
  case 2:
    RGB[0]= 0.f; // red
    RGB[1]= MAX_COLOR_VALUE; // green
    RGB[2]= value*MAX_COLOR_VALUE; // blue
    break;
  case 3:
    RGB[0]= 0.f; // red
    RGB[1]= (1.0f - value)*MAX_COLOR_VALUE; // green
    RGB[2]= MAX_COLOR_VALUE; // blue
    break;
  default:
    RGB[0]= value * MAX_COLOR_VALUE; // red
    RGB[1]= 0.f; // green
    RGB[2]= MAX_COLOR_VALUE; // blue
    break;
  }
  return;
}

// Rainbow color mapping, 2nd version
inline void
GetPseudoColor006(float valueV, float minVal, float maxVal,
		  CColor &RGB)
{
  if (isnan_local(valueV)) {
    RGB[0]=RGB[1]=RGB[2]=0;
    return;
  }
  if (valueV < minVal) valueV = minVal;
  if (valueV > maxVal) valueV = maxVal;

  float ratio = (valueV - minVal)/(maxVal - minVal);
  float value = 1.f-ratio;
  const float MAX_COLOR_VALUE = 0.999f;
  float val4 = value * 4.0f;
  value = val4 - (int)val4;
  switch ((int)(val4)) {
  case 0:
    RGB[0] = ( MAX_COLOR_VALUE); // red
    RGB[1] = ( (value)*MAX_COLOR_VALUE); // green
    RGB[2] = ( 0.f); // blue
    break;
  case 1:
    RGB[0] = ( (1.0f - value)*MAX_COLOR_VALUE); // red
    RGB[1] = ( MAX_COLOR_VALUE); // green
    RGB[2] = ( 0.f); // blue
    break;
  case 2:
    RGB[0] = ( 0.f); // red
    RGB[1] = ( MAX_COLOR_VALUE); // green
    RGB[2] = ( value*MAX_COLOR_VALUE); // blue
    break;
  case 3:
    RGB[0] = ( 0.f); // red
    RGB[1] = ( (1.0f - value)*MAX_COLOR_VALUE); // green
    RGB[2] = ( MAX_COLOR_VALUE); // blue
    break;
  default:
    RGB[0] = ( value * MAX_COLOR_VALUE); // red
    RGB[1] = ( 0.f); // green
    RGB[2] = ( MAX_COLOR_VALUE); // blue
    break;
  }
  return;
}


inline void
GetPseudoColor(int alg, float valueV, float minVal, float maxVal, // input values
	       CColor &RGB) // output color
{
  switch(alg) {
  case 0: {
    float lum = RGB.Luminance();
    RGB[0] = RGB[1] = RGB[2] = lum;
    break;
  }
  case 1: GetPseudoColor001(valueV, minVal, maxVal, RGB); break;
  case 2: GetPseudoColor002(valueV, minVal, maxVal, RGB); break;
  case 3: GetPseudoColor003(valueV, minVal, maxVal, RGB); break;
  case 4: GetPseudoColor004(valueV, minVal, maxVal, RGB); break;
  case 5: GetPseudoColor005(valueV, minVal, maxVal, RGB); break;
  case 6: GetPseudoColor006(valueV, minVal, maxVal, RGB); break;
  default: {
    FATAL << "Pseudocolor technique = " << alg << " not implemented .. exiting. "
	  << endl << flush;
    FATAL_ABORT;
  }
  }
  return;
}

#endif // __COLORFS_H__

// ===================================================================
//
//  TAlloc.h
//  Header file with functions for data allocations and deallocations
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
//  Initial coding by Jiri Filip, UTIA CAS CR

#ifndef __TALLOC_H__
#define __TALLOC_H__

#include "configh.h"

__BEGIN_GOLEM_HEADER

//allocations
float* allocation1(int nrl,int nrh);
float** allocation2(int nrl,int nrh,int ncl,int nch);
float*** allocation3(int nmf,int nml,int nrl,int nrh,int ncl,int nch);

//deallocations
void freemem1(float *m,int nrl,int nrh);
void freemem2(float **m,int nrl,int nrh,int ncl,int nch);
void freemem3(float ***m,int nmf,int nml,int nrl,int nrh,int ncl,int nch);

__END_GOLEM_HEADER

#endif //__TALLOC_H__


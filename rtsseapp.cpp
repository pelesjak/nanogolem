// nanoGOLEM headers
#include "rtsseapp.h"
#include "color.h"
#include "configg.h"
#include "dotcount.h"
#include "environ.h"
#include "errorg.h"
#include "ray.h"
#include "rnd.h"
#include "scene.h"
#include "sseutils.h"
#include "vector3.h"

__BEGIN_GOLEM_SOURCE

void CRT_SSE_App::ComputeColor(CRayPacket &rayPacket,
                               CRPHitPointsInfo &packetInfo, CColor *colors) {

  // return if no intersection occured
  if (!packetInfo.intersected) {
    return;
  }

  float greyValues[4] __attribute__((aligned(16)));
  __m128 greyValue;
  CColorSSE d;

  // -----------------------------------------------------------------
  // use the new property of the surface returning standard color
  __m128 minCosAngle = _mm_set1_ps(0.05f);

  packetInfo.GetNormals();

  // We have to call GetNormal() as it is not computed
  __m128 dotProd = DotProd(packetInfo.normals, rayPacket.dir);

  // whether to show the surfaces with opposite normals as well -
  // for scene debugging!
  if (_showBackFaceSurfaces) {
    greyValue = _mm_max_ps(minCosAngle, _mm_abs_ps(dotProd));
  } else {
    __m128 cmp = _mm_cmpge_ps(dotProd, _MM_ZERO);
    // surface normal oriented towards the viewer
    greyValue = _mm_cndval_ps(cmp, _mm_max_ps(minCosAngle, dotProd), _MM_ZERO);
  }
  _mm_store_ps(greyValues, greyValue);
  for (int i = 0; i < 4; i++) {
    if ((packetInfo.intersected >> i) & 1) {
      colors[i] = greyValues[i] * _imgHdrMult;
    }
  }
  return;
}

bool CRT_SSE_App::Init(CWorld *world, string &outputName) {
  // init previous classes
  if (CRT_CoreWithCamera_App::Init(world, outputName))
    return true; // error occured

  _environment->GetBool("RTCore.showBackfaceSurfaces", _showBackFaceSurfaces);
  _environment->GetFloat("Image.HDR.multiplier", _imgHdrMult);

  STATUS << "Ray tracing with SSE based 2x2 packets" << endl << flush;
  return false; // ok
}

bool CRT_SSE_App::RunCommonCompute() {
  // Load flags
  bool olb = false, shadowRaysAllowed = false;
  _environment->GetBool("SetCamera.originLeftBottom", olb);
  _environment->GetBool("Application.shadowRays", shadowRaysAllowed);

  // Start the timer
  _timer.StartHighRes();

  // rendering of the image by ray tracing itself
  int xmin = 0, ymin = 0, xmax, ymax;
  xmax = _image->GetWidth();
  ymax = _image->GetHeight();

  int y, x;
  CVector3D dir = Normalize(_camera.dir);
  CVector3D up = _camera.up;
  up.Normalize();
  CVector3D right = CrossProd(dir, up);
  right.Normalize();
  up = CrossProd(right, dir);
  up.Normalize();

  float width = float(_image->GetWidth());
  float xcenter = width * 0.5f;
  float height = float(_image->GetHeight());
  float ycenter = height * 0.5f;
  float alpha = atan(height / width);
  float fovy = sin(alpha) * _camera.FOV;
  float fovx = cos(alpha) * _camera.FOV;
  CVector3D right_px = ((2 * tan(fovx / 2)) / width) * right;
  CVector3D up_px = ((2 * tan(fovy / 2)) / height) * up;

  if (olb) {
    up_px = -up_px;
  }

  CRayPacket rayPacket;
  CRPHitPointsInfo packetInfo;
  CColor colors[4];

  CRay shadowRay;
  CRayPacket shadowPacket;
  CRPHitPointsInfo shadowPacketInfo;
  CHitPointInfo shadowInfo;
  CColor shadowColor = _scene->bgColor * 0.1;
  float intersections[12] __attribute__((aligned(16)));

  // -------------------------------------------------------
  this->primHit = 0;
  this->primMiss = 0;

  const int cntMaxPixels = (ymax - ymin) * (xmax - xmin);
  const int cntDots = 100;
  CDotsCounter *dotCount =
      new CPercentagePrintCounter(cntMaxPixels / 4, cntDots, cout);

  for (y = ymin; y < ymax; y += 2) {
    for (x = xmin; x < xmax; x += 2) {
      CVector3D raydir = dir;
      CVector3D rX = (x - xcenter) * right_px;
      CVector3D rY = (y - ycenter) * up_px;
      raydir += rX + rY;

      // Init directions of rays in packet
      rayPacket.dir.x =
          _mm_set_ps(raydir[0] + right_px[0] + up_px[0], raydir[0] + up_px[0],
                     raydir[0] + right_px[0], raydir[0]);
      rayPacket.dir.y =
          _mm_set_ps(raydir[1] + right_px[1] + up_px[1], raydir[1] + up_px[1],
                     raydir[1] + right_px[1], raydir[1]);
      rayPacket.dir.z =
          _mm_set_ps(raydir[2] + right_px[2] + up_px[2], raydir[2] + up_px[2],
                     raydir[2] + right_px[2], raydir[2]);

      rayPacket.avgDir = raydir + (0.5 * right_px) + (0.5 * up_px);

      // Normalize afterwards
      rayPacket.dir.Normalize();
      rayPacket.avgDir.Normalize();

      // Set origin
      rayPacket.origin = _camera.loc;

      // Init hit points info
      packetInfo.Init(&rayPacket);

      unsigned hits = _asds->FindNearest(rayPacket, packetInfo);
      unsigned currentHits = 0;

      for (int i = 0; i < 4; i++) {
        if ((hits >> i) & 1) {
          primHit++;
          currentHits++;
          colors[i] = 0;
        } else {
          primMiss++;
          colors[i] = _scene->bgColor;
        }
      }

      if (hits && shadowRaysAllowed) {
        /* if (currentHits == 4) {
          packetInfo.Extrap();
          packetInfo.GetNormals();
          shadowPacket.origin = packetInfo.intersections;
          __m128 _maxt = _mm_set_ps(
              (hits & 1) ? 0.999 : -1, ((hits >> 1) & 1) ? 0.999 : -1,
              ((hits >> 2) & 1) ? 0.999 : -1, ((hits >> 3) & 1) ? 0.999 : -1);
          for (auto l : _scene->lights) {
            shadowPacket.dir = CVectorSSE3D(l->pos) - shadowPacket.origin;
            shadowPacketInfo.Init(&shadowPacket);
            shadowPacketInfo.maxt = _maxt;
            hits = _asds->FindNearest(shadowPacket, shadowPacketInfo);
            if (hits < _MM_MASK_TRUE) {
              CPointLight *pl = (CPointLight *)l;
              pl->ComputeColor(shadowPacket.origin, packetInfo.normals,
                               _camera.loc, colors, hits);
            }
          }
        } else  */{
          _mm_store_ps(intersections, packetInfo.intersections.x);
          _mm_store_ps(intersections + 4, packetInfo.intersections.y);
          _mm_store_ps(intersections + 8, packetInfo.intersections.z);

          for (int i = 0; i < 4; i++) {
            if ((hits >> i) & 1) {
              colors[i] = shadowColor;
              CVector3D whereFrom = CVector3D(
                  intersections[i], intersections[4 + i], intersections[8 + i]);
              for (auto l : _scene->lights) {
                CVector3D shadowDir = l->pos - whereFrom;
                shadowRay.Init(whereFrom, shadowDir, true);
                shadowInfo.SetForFindNearest(shadowRay);
                shadowInfo.SetMaxT(0.999);
                if (!_asds->FindNearest(shadowRay, shadowInfo)) {
                  CPointLight *pl = (CPointLight *)l;
                  colors[i] += pl->ComputeColor(
                      whereFrom, packetInfo.normalsSeq[i], _camera.loc);
                }
              }
              primHit++;
            } else {
              colors[i] = _scene->bgColor;
              primMiss++;
            }
          }
        }
      } else {
        ComputeColor(rayPacket, packetInfo, colors);
      }

      /*

        packetInfo.Extrap();

        _mm_store_ps(intersections, packetInfo.intersections.x);
        _mm_store_ps(intersections + 4, packetInfo.intersections.y);
        _mm_store_ps(intersections + 8, packetInfo.intersections.z);

        for (int i = 0; i < 4; i++) {
          if ((hits >> i) & 1) {
            colors[i] = shadowColor;
            CVector3D whereFrom = CVector3D(
                intersections[i], intersections[4 + i], intersections[8 + i]);
            for (auto l : _scene->lights) {
              CVector3D shadowDir = l->pos - whereFrom;
              shadowRay.Init(whereFrom, shadowDir, true);
              shadowInfo.SetForFindNearest(shadowRay);
              shadowInfo.SetMaxT(0.999);
              if (!_asds->FindNearest(shadowRay, shadowInfo)) {
                CPointLight *pl = (CPointLight *)l;
                colors[i] += pl->CalculateColor(
                    whereFrom, packetInfo.normalsSeq[i], _camera.loc);
              }
            }
            primHit++;
          } else {
            colors[i] = _scene->bgColor;
            primMiss++;
          }
        }
      */

      _image->SetRGB_XY(x, y, colors[0][0], colors[0][1], colors[0][2]);
      _image->SetRGB_XY(x + 1, y, colors[1][0], colors[1][1], colors[1][2]);
      _image->SetRGB_XY(x, y + 1, colors[2][0], colors[2][1], colors[2][2]);
      _image->SetRGB_XY(x + 1, y + 1, colors[3][0], colors[3][1], colors[3][2]);

      // dotCount->Inc();
    } // for x
  }   // for y

  _timer.StopHighRes();

  delete dotCount;

  STATUS.precision(10);

  STATUS << endl
         << endl
         << "PrimHits= " << primHit << " primMis= " << primMiss
         << " primTotal= " << primHit + primMiss << endl;
  STATUS << "Render time ms: " << _timer.GetHighResIntervalMili() << endl;

  long long packetCnt = cntMaxPixels / 4;
  double intersectionsPerPacket =
      (double)packetInfo.GetIntersectionCounter() / (double)(packetCnt);
  double stepsPerPacket = (double)packetInfo.GetSteps() / (double)(packetCnt);
  STATUS << "Average intersection calculations per ray-packet: "
         << intersectionsPerPacket << endl;
  STATUS << "Average steps through ASDS per ray-packet: " << stepsPerPacket
         << endl;

  if (statsFile.is_open()) {
    statsFile << "Render time ms: " << _timer.GetHighResIntervalMili() << ", ";
    statsFile << "Intersections per packet: " << intersectionsPerPacket << ", ";
    statsFile << "Steps per packet: " << stepsPerPacket << ", ";
    statsFile << "Packet count: " << packetCnt;
  }

  return false; // OK
}

// --------------------------------------------------------------------------
// This computes the optimization
bool CRT_SSE_App::Run() {
  if (RunCommonPrepare())
    return true; // problem

  bool ok;
  ok = RunCommonCompute();

  if (ok)
    return ok; // problem

  bool HDRflag = true; // true .. save HDR image, false ... LDR image

  string ftype;
  _environment->GetString("OutputFileType", ftype);
  if ((ftype.compare("png") == 0) || (ftype.compare("PNG") == 0)) {
    HDRflag = false;
  }

  // Save image from this single camera
  SaveImage(HDRflag);

  // Save the image to the file and deallocate the dynamic variables
  if (RunCommonFinish())
    return true; // problem

  return ok;
}

__END_GOLEM_SOURCE
#include "light.h"
#include "color.h"
#include "configg.h"
#include "vector3.h"
#include <xmmintrin.h>

__BEGIN_GOLEM_SOURCE

CColor KD = CColor(0, 0.4, 0.98);
CColor KS = 0.8;
float SHINE = 10;

void SetMaterialPhong(CColor &kd, float ks, float shine) {
  KD = kd;
  KS = ks;
  SHINE = shine;
}

CVector3D Reflect(const CVector3D &normal, const CVector3D &in) {
  return in - normal * (2.0 * DotProd(normal, in));
}

CVectorSSE3D Reflect(CVectorSSE3D &normal, CVectorSSE3D in) {
  return in - normal * (_mm_mul_ps(DotProd(normal, in), _mm_set1_ps(2.0)));
}

CColor CPointLight::ComputeColor(CVector3D &point, CVector3D &norm,
                                 CVector3D &camPosition) {
  CVector3D L = pos - point;
  L.Normalize();
  CVector3D R = Reflect(norm, -L);
  R.Normalize();
  CVector3D V = camPosition - point;
  V.Normalize();

  float cosAlpha = std::max((float)0, DotProd(norm, L));
  float cosBeta = std::max((float)0, DotProd(R, V));

  CColor diffuse = KD * (col * cosAlpha);
  CColor specular = KS * (col * pow(cosBeta, SHINE));
  return diffuse + specular;
}

void CPointLight::ComputeColor(CVectorSSE3D &point, CVectorSSE3D &norm,
                               CVector3D &camPosition, CColor *colors,
                               unsigned intersected) {
  CVectorSSE3D L = pos - point;
  L.Normalize();
  CVectorSSE3D R = Reflect(norm, -L);
  R.Normalize();
  CVectorSSE3D V = camPosition - point;
  V.Normalize();

  __m128 cosAlpha = _mm_max_ps(_MM_ZERO, DotProd(norm, L));
  __m128 cosBeta = _mm_max_ps(_MM_ZERO, DotProd(R, V));
  cosBeta = _mm_mul_ps(cosBeta, cosBeta);
  float cosAlphas[4] __attribute__((aligned(16)));
  float cosBetas[4] __attribute__((aligned(16)));

  _mm_store_ps(cosAlphas, cosAlpha);
  _mm_store_ps(cosBetas, cosBeta);

  CColor diffuse = KD * col, specular = KS * col;

  for (int i = 0; i < 4; i++) {
    if (!((intersected >> i) & 1)) {
      colors[i] += diffuse * cosAlphas[i] + specular * cosBetas[i];
    }
  }
}

__END_GOLEM_SOURCE
